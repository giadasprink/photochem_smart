def run_many(haze=False, dofractals=True, shortfname=True, difflbl = True, gencloud = True, makeplots = True):
    #e.g. run_many.run_many(haze=True)

    import numpy
    from numpy import loadtxt
    #load .pt files
    files = numpy.loadtxt('/astro/users/giada/photochem_smart/photchem/list_files.lst', dtype='a')
    #load hcaer files
    if haze:
        files2 = numpy.loadtxt('/astro/users/giada/photochem_smart/haze_io/hcaer/list_files.lst', dtype='a')
    from interface import smart_interface_haze 
   

    for x in range(0, numpy.size(files)):
        photfile = 'photchem/'+files[x]
        if haze: hazefile = files2[x]
        tag = files[x]
        if haze: tag = 'HAZE_'+files[x]
        if difflbl: difflbltag = files[x]
        if not difflbl: difflbltag = ''
        if not haze: smart_interface_haze.smart_interface_haze(photfile, tag, genlblabc=False, runsmart=False, forwardscatt = False, hazecloud = False, shortfname=shortfname, difflbl = difflbl, difflbltag = difflbltab, gencloud=gencloud, makeplots=makeplots)
        if haze: smart_interface_haze.smart_interface_haze(photfile, tag, hcaerfile=hazefile, genlblabc=False, runsmart=False, forwardscatt = False, 
                                                           fractally=dofractals, shortfname=shortfname, difflbl=difflbl, 
                                                           difflbltag = difflbltag, gencloud=gencloud, makeplots=makeplots)
