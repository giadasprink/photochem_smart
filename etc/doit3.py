def run_many(haze=False):
    import numpy
    from numpy import loadtxt
    #load .pt files
    files = numpy.loadtxt('/astro/users/giada/photochem_smart/photchem/list_files.lst', dtype='a')
    #load hcaer files
    if haze:
        files2 = numpy.loadtxt('/astro/users/giada/photochem_smart/haze_io/hcaer/list_files.lst', dtype='a')
    from interface import smart_interface_haze 
 

    for x in range(0, numpy.size(files)):
        photfile = 'photchem/'+files[x]
        if haze: hazefile = 'haze_io/hcaer/'+files2[x]
        tag = files[x]
        if haze: tag = 'HAZE_'+files[x]
        if not haze: smart_interface_haze.smart_interface_haze(photfile, tag, genlblabc=False, runsmart=False)
        if haze: smart_interface_haze.smart_interface_haze(photfile, tag, hcaerfile=hazefile, genlblabc=False, runsmart=False)
