PRO write_photo_script, rmix=rmix, flux=flux, filename=filename

;This should get pythonized sometime and made more
;general and user friendly but it's saved me so much time that
;I'm including the IDL version for now since somebody
;else may find it useful even in this somewhat convoluted and
;obnoxious form...
;-Giada

;it allows users to create a zillions of photo runs with varied parameters
;and it puts the output from those photo runs in a few places.
;the script generated contains lines of perl code that edit
;species.dat, PLANET.dat, and input_photochem.dat for each photo run, then reverts back to 
;default parameters when all runs are finished.
  ;makes a new folder labled with parameters you're looping through for
  ;   each photo run in /astro/users/giada/photo/out/archean_sun/
  ;all the .out files get put into that folder in /astro/users/giada/photo/out/archean_sun/
  ;the profile.pt files get put into /astro/users/giada/photochem_smart/photchem/
  ;the hcaer files get put into /astro/users/giada/photochem_smart/haze_io/hcaer/
  ;the out.rates files get put into /astro/users/giada/archean_earth/cloud_files/hcaer
;edit the paths below accordingly to choose different locations...

;edit the arrays under the "EDIT THIS STUFF" label to 
;choose what parameters to loop through.  Currently allows
;for ch4 mixing ratio or flux, pressure (bars), oxygen mixing ratio,
;time (in Ga), and hcaer particle density

;KEYWORDS
;rmix - set this keyword to make the ch4 amounts a mixing ratio
;flux - OR set this keyword to make the ch4 amounts a flux
;filename - default output for this is photo_script.csh.
;           set this to something else to change it.
;
;TO USE THE OUTPUT:
;go to your photo folder and then type
;$ chmod +x photo_script.csh (only need to do first time it's generated)
;$ ./photo_script.csh


  IF not keyword_set(rmix) and not keyword_set(flux) THEN BEGIN
     print, 'do you want methane mixing ratio or flux?'
     input = ''
     READ, input, prompt = 'Type rmix/flux: '
     IF input EQ 'rmix' THEN type = 'rmix'
     IF input EQ 'flux' THEN type = 'flux'
     IF input NE 'rmix' and input NE 'flux' THEN BEGIN
        print, 'invalid input.'
        stop
     ENDIF
  ENDIF

  IF keyword_set(rmix) THEN type = 'rmix'
  IF keyword_set(flux) THEN type = 'flux'

;----------EDIT THIS STUFF-----------------
  pressures = ['1.013', '0.500' ]
  oxygen = ['1.0E-08', '1.0E-07', '1.0E-06', '1.0E-05']
  times = ['0.0', '2.5', '2.7']
  dens = ['0.63',  '1.0']
                                ;IF type eq 'rmix' THEN methane = ['1.0E-02', '2.0E-03', '2.5E-03', '3.0E-03', '3.5E-03'] ;rmix
                                ;IF type eq 'rmix' THEN methane = ['2.0E-03', '2.1E-03', '2.2E-03', '2.3E-03','2.4E-03', '2.5E-03', '2.6E-03', '2.7E-03', '2.8E-03', '2.9E-03', '3.0E-03'] ;rmix
  IF type eq 'rmix' THEN methane = [ '0.5E-03', '1.0E-03', '2.0E-03','2.3E-03','2.5E-03', '2.7E-03','3.0E-03','4.0E-03', '6.0E-03' ] ;rmix

;for fluxes they are e+11, but don't write that part
  IF type eq 'flux' THEN methane = ['3.410', '1.000', '0.300', '5.000'] ;fluxes times E+11
 
;----------------------------------------------


  defaultppmv = '1.0E-02'
  defaultpress = '1.013'
  defaultga = '0.0'
  defaultflux = '3.410'
  defaultdens = '0.63'
  defaultoxy = '1.0E-08'


  imacounter= 0
  madefile=0

  IF type eq 'rmix'  then lastmethane = defaultppmv
  IF type eq 'flux'  then lastmethane = defaultflux
  lastpress = defaultpress
  lasttime = defaultga
  lastdens = defaultdens
  lastoxy  = defaultoxy
  
  IF not keyword_set(filename) THEN file = '/astro/users/giada/photo/photo_script.csh'
  IF keyword_set(filename) THEN file = '/astro/users/giada/photo/'+filename+'.csh'
  
  for gg =0, n_elements(times) -1 do begin
     for mm=0, n_elements(methane) -1 do begin
        for pp=0, n_elements(pressures)-1 do begin
           for dd=0, n_elements(dens)-1 do begin
              for ox=0, n_elements(oxygen)-1 do begin

;billion years ago
                 timechange = "perl -pi -e 's/"+lasttime+"      = TIMEGA - time in Ga, for modifying the solar flux/"+times[gg]+"      = TIMEGA - time in Ga, for modifying the solar flux/g' /astro/users/giada/photo/INPUTFILES/PLANET.dat"

;hcaer density
                 denschange =  "perl -pi -e 's/HCDENS=    "+lastdens+"/HCDENS=    "+dens[dd]+"/g' /astro/users/giada/photo/INPUTFILES/input_photchem.dat"

;oxygen amount
                 oxychange = "perl -pi -e 's/O2         LL  2 0 0 0 0 0    1     0.      "+lastoxy+"/O2         LL  2 0 0 0 0 0    1     0.      "+oxygen[ox]+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"

;atmos pressure
                 pchange = "perl -pi -e 's/"+lastpress+"    = P0  = surface pressure of modern Earth in atm/"+pressures[pp]+"    = P0  = surface pressure of modern Earth in atm/g' /astro/users/giada/photo/INPUTFILES/PLANET.dat"

;amount of methane ppm
                 IF type eq 'rmix' THEN BEGIN
                    ch4change = "perl -pi -e 's/CH4        LL  0 4 1 0 0 0    1     0.      "+lastmethane+"/CH4        LL  0 4 1 0 0 0    1     0.      "+methane[mm]+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"
                 ENDIF

;amount of methane flux
                 IF type eq 'flux' THEN BEGIN
                    IF gg eq 0 and mm eq 0 and pp eq 0 then begin                
                       ch4change = "perl -pi -e 's/CH4        LL  0 4 1 0 0 0    1     0.      1.0E-02 "+lastmethane+"/CH4        LL  0 4 1 0 0 0    2     0.      1.0E-02 "+methane[mm]+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"            
                    ENDIF ELSE BEGIN             
                       ch4change = "perl -pi -e 's/CH4        LL  0 4 1 0 0 0    2     0.      1.0E-02 "+lastmethane+"/CH4        LL  0 4 1 0 0 0    2     0.      1.0E-02 "+methane[mm]+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"
                    ENDELSE

                 ENDIF
                 copy = 'cp /astro/users/giada/photo/INPUTFILES/DEFAULT_planet.dat /astro/users/giada/photo/INPUTFILES/PLANET.dat'
                 copy2 = 'cp  /astro/users/giada/photo/INPUTFILES/DEFAULT_species.dat /astro/users/giada/photo/INPUTFILES/species.dat'
                 copy3 = 'cp  /astro/users/giada/photo/INPUTFILES/DEFAULT_input_photchem.dat /astro/users/giada/photo/INPUTFILES/input_photchem.dat'

                 perl_lines = [timechange, denschange, oxychange, pchange, ch4change]
                 IF madefile eq 0 then perl_lines = [copy, copy2, copy3, timechange, denschange, oxychange, pchange, ch4change]
                 run = strarr(10)

                 run[0]= ''
                 run[1] = "TOTCdev"
                 run[2] ="mkdir /astro/users/giada/photo/out/archean_sun/"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3_/"
                 run[3] ="cp out.out /astro/users/giada/photo/out/archean_sun/"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3/out.out"
                 run[4] ="cp profile.pt /astro/users/giada/photochem_smart/photchem/"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3.pt"
                 run[5] ="cp hcaer.out /astro/users/giada/photochem_smart/haze_io/hcaer/hcaer_"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3_.out"
                 run[6] ="cp hcaer2.out /astro/users/giada/photochem_smart/haze_io/hcaer/hcaer2_"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3_.out"
                 run[7] ="cp out.rates /astro/users/giada/archean_earth/cloud_files/hcaer/OUTPUT"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3_.out"
                 run[8] ="cp *out* /astro/users/giada/photo/out/archean_sun/"+times[gg]+"Ga_"+methane[mm]+"ch4_"+type+"_"+pressures[pp]+"bar_"+oxygen[ox]+"o2_"+dens[dd]+"gcm3_"
                 run[9]= ''

;code to write files

                 IF madefile ne 0 then begin
                    openw, 1, file, /append
                    for j =0, n_elements(perl_lines) -1 do begin 
                       size = strlen(perl_lines[j])
                       printf, 1, perl_lines[j], format='(a'+strtrim(size,1)+')'
                    endfor

                    for w =0, n_elements(run) -1 do begin 
                       size = strlen(run[w])
                       printf, 1, run[w], format='(a'+strtrim(size,1)+')'
                    endfor
                    close,1
                 ENDIF

                 IF madefile eq 0 then begin
                    openw, lun, file, /get_lun
                    for j =0, n_elements(perl_lines) -1 do begin 
                       size = strlen(perl_lines[j])
                       printf, lun, perl_lines[j], format='(a'+strtrim(size,1)+')'
                    endfor

                    for w =0, n_elements(run) -1 do begin 
                       size = strlen(run[w])
                       printf, lun, run[w], format='(a'+strtrim(size,1)+')'
                    endfor
                    free_lun, lun
                    madefile = 1
                 endif


                 imacounter = imacounter + 1

                 lastmethane = methane[mm]
                 lastpress = pressures[pp]
                 lasttime = times[gg]
                 lastdens = dens[dd]
                 lastoxy = oxygen[ox]

              endfor
           endfor
        endfor
     endfor
  endfor


;restore defaults:
;billion years ago
  defaulttime = "perl -pi -e 's/"+lasttime+"      = TIMEGA - time in Ga, for modifying the solar flux/"+defaultga+"      = TIMEGA - time in Ga, for modifying the solar flux/g' /astro/users/giada/photo/INPUTFILES/PLANET.dat"

;atmos pressure
  defaultp = "perl -pi -e 's/"+lastpress+"    = P0  = surface pressure of modern Earth in atm/"+defaultpress+"    = P0  = surface pressure of modern Earth in atm/g' /astro/users/giada/photo/INPUTFILES/PLANET.dat"

;amount of methane ppm
  IF type eq 'rmix' THEN BEGIN
     defaultch4 = "perl -pi -e 's/CH4        LL  0 4 1 0 0 0    1     0.      "+lastmethane+"/CH4        LL  0 4 1 0 0 0    1     0.      "+defaultppmv+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"
  ENDIF

;amount of methane flux
  IF type eq 'flux' THEN BEGIN
     defaultch4 = "perl -pi -e 's/CH4        LL  0 4 1 0 0 0    2     0.      1.0E-02 "+lastmethane+"/CH4        LL  0 4 1 0 0 0    1     0.      1.0E-02 "+defaultflux+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"
  ENDIF

;hcaer density
  defaultdens =  "perl -pi -e 's/HCDENS=    "+lastdens+"/HCDENS=    "+defaultdens+"/g' /astro/users/giada/photo/INPUTFILES/input_photchem.dat"

;oxygen amount
  defaultoxy = "perl -pi -e 's/O2         LL  2 0 0 0 0 0    1     0.      "+lastoxy+"/O2         LL  2 0 0 0 0 0    1     0.      "+defaultoxy+"/g' /astro/users/giada/photo/INPUTFILES/species.dat"


  
  defaults = [defaultp, defaultch4, defaulttime, defaultdens, defaultoxy]

  openw, 1, file, /append
  for j =0, n_elements(defaults) -1 do begin 
     size = strlen(defaults[j])
     printf, 1, defaults[j], format='(a'+strtrim(size,1)+')'
  endfor
  close,1

  print, 'guess what?  this script generated ' + strtrim(imacounter,1) + ' photochemical model runs!'

;done!

END

