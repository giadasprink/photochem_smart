import numpy
from numpy import loadtxt
files = numpy.loadtxt('/astro/users/giada/photochem_smart/photchem/list_files.lst', dtype='a')
from interface import smart_interface

for x in range(0, numpy.size(files)):
    photfile = 'photchem/'+files[x]
    tag = files[x]
    smart_interface(photfile, tag, genlblabc=False, runsmart=False)
