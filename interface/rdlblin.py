# -*- coding: iso-8859-1 -*-
#file: rdlblin

def rdlblin(filename = 'lblabc_io/lblin.txt'):
    
    """Reads input values for write_lblabc

    This function reads input values used by write_lblabc to 
    generate runscripts for LBLABC. LBLABC will calculate the 
    line-by-line absorption coefficients for an input gas whose 
    mixing ratios (and pressure-temperature profile) are known. 

    For default values see 'lblabc_io/lblin_default.txt'
    
    Parameters
    ----------
    filename : string, optional
        path and name of input values for write_lblabc
        Default: 'lblabc_io/lblin.txt'

    Returns
    ----------
    dmolwgt : float, optional
        Default mean molecular weight of the atmosphere (g/mol)
        Note that usually this can be recovered from input photo file
        Default: 29.
    minwn : integer, optional
        Minimum wavenumber cutoff for LBLABC
        Default: 300 (33.3 um)
    maxwn : integer, optional
        Maximum wavenumber cutoff for LBLABC
        Default: 100000 (0.1 um)   
    grav : float, optional
        Gravitational acceleration (m/s2)
        Default: 9.8
    radius : float, optional
        Planetary radius (km)
        Default: 6371.
    col_p : integer, optional
        Column of input file containing pressure values
        Default: 1
    col_t : integer, optional
        Column of input file containing temperature values
        Default: 2
    scaleP : float, optional
        Pressure scaling factor used by LBLABC
        Default: 1.0
    n_t_prof : integer, optional
        Number of temperature profiles to calculate .abs coefficients for
        Default: 3
    t_offset : float, optional
        Temperature profile offset (K)
        Default: 25.0
    n_broad : integer, optional
        Number of foreign broadening gases
        Default: 0
    scale_rmix : float, optional
        Scaling factor for mixing ratio
        Default: 1.0
    max_width : float, optional
       Maximum line width
       Default: 1000.0
    min_tau : float, optional
       Minimum column optical depth
       Default: 1.e-5
    out_dir : string, optional
       Directory to put output .abs file (BIG!)
       Default: 'lblabc_io/output/'
    script_dir : string, optional
       Directory to write the runlblabc script to
       Default: 'lblabc_io/runlblabc/'
    skip : integer, optional
       Number of lines LBLABC should skip at top of .atm file
       Default: 1
    rmix_type : integer, optional
       Type of mixing ratio - volume (1) or mass (2)
       Default: 1
    par_file : string, optional
       Path and filename of HITRAN parameter file (.par)
       Default: 'fixed_input/hitran08.par'
    fundamntl_file : string, optional
       Path and filename of fundamntl file (.dat)
       Default: 'fixed_input/fundamntl.dat'
    atm_dir : string, optional
       Directory to look for .atm files
       Default: 'atm/'

    Revision History
    ----------
    Written by E. Schwieterman February, 2014
    Examples only work for default values.
    02/24/2014 - Now reads atm directory from input file - EWS

    Examples
    ----------
    >>> dmolwgt, minwn, maxwn, grav, radius, col_p, col_t, scaleP, n_t_prof,\
    t_offset, n_broad, scale_rmix, maxwidth, min_tau, out_dir, script_dir,\
    skip, rmix_type, par_file, hitran_tag, fundamntl_file, atm_dir \
    = rdlblin.rdlblin(filename = 'lblabc_io/lblin_default.txt')
    >>>print minwn
    300.0

    """
    
    # --------import numpy---------------------------------#
    import numpy as np
    #------------------------------------------------------#

    #-----generate from text-------------------------------#
    input = np.genfromtxt(filename,dtype=None,comments='#')
    #------------------------------------------------------#

    #-----------name variables and correct type------------#
    dmolwgt =        float(input[0])
    minwn =          float(input[1])
    maxwn =          float(input[2])
    grav =           float(input[3])
    radius =         float(input[4])
    col_p =          int(input[5])
    col_t =          int(input[6])
    scaleP =         float(input[7])
    n_t_prof =       int(input[8])
    t_offset =       float(input[9])
    n_broad =        int(input[10])
    scale_rmix =     float(input[11])
    maxwidth =       float(input[12])
    min_tau =        float(input[13])
    out_dir =        str(input[14])
    script_dir =     str(input[15])
    skip =           int(input[16])
    rmix_type =      int(input[17])
    par_file =       str(input[18])
    hitran_tag =     str(input[19])
    fundamntl_file = str(input[20])
    atm_dir =        str(input[21])
    #--------------------------------------------------------#

    #----------------return values---------------------------#
    return  dmolwgt, minwn, maxwn, grav, radius, col_p, col_t,\
            scaleP, n_t_prof, t_offset, n_broad, scale_rmix, maxwidth,\
            min_tau, out_dir, script_dir, skip, rmix_type, par_file, \
            hitran_tag, fundamntl_file, atm_dir
    #---------------------------------------------------------#
