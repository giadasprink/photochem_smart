# -*- coding: iso-8859-1 -*-
#file: read_photo

def read_photo(infile, tag, plvls = 60, scaleP = 1.e5, atm_dir = 'atm/', addn2 = True):
    """Reads/parses a photochemical model output file and produces a SMART-readable .atm file.
 
    The function reads the photochemical input file with an arbitrary number of gas mixing ratios, 
    locates the pressure and temperature columns, dynamically creates an array to accommodate all 
    the gas mixing ratios, matches the gas label from the photochemical output with a preloaded list 
    of gases that include all the HITRAN gases, creates a new grid of pressures equally spaced in 
    logarithmic pressure, interpolates the temperature and gas mixing ratios to the new pressure grid, 
    explicitly calculates the N2 mixing ratio for each level by assuming it is the remainder of all 
    gases in that level, and dynamically calculates the mean molecular weight of the atmosphere. 
    It creates the new .atm file and returns info necessary to write the LBLABC and SMART scripts. 
    Replaces profile.f, pgrid.f, and xyinterp.f.
     
    Parameters
    ----------
    infile : string
        Photochemical model output file. Should have one header row.
        Ex: 'ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux.GHGprofile.pt'
    tag : string
        A unique indentifier used as a substring in all outputs
        Ex: 'ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux'
    plvls : integer, optional
        Number of pressure levels in new .atm file. SMARTS chokes >~62
        Default: 60 
    scaleP : float, optional
        Scaling factor for pressure.
        Default: 1.e5 (bars to pascals)
    atm_directory : string, optional
        Directory to place new .atm file
        Default: 'atm/'
    addn2 : bool, optional
        Whether or not to use N2 as a filler gas
        Default: True

    Returns
    ---------- 
    gas_code : list
        Matched list contains gas codes from photochemical file
    atm_pos : list
        Contains new column positions of gases in gas_code in output .atm file
    molwgt : float
        Dynamically calculated mean molecular weight of the atmosphere
    atm_file : string
        Name and path to output .atm file.

    Notes
    ---------- 
    Writes interpolated temperature and mixing ratio profiles to new pressure
    grid in the output .atm file.

    Revision History
    ---------- 
    Written by E. Schwieterman January, 2014
    - changed output column_pos (original column position in photochem file) to atm_pos (position in 
      output .atm file) matched against gas_code list. - EWS 1/27/2014

    Examples   
    ---------- 
    >>> gas_code, column_pos, molwgt, filename = \
    convert_photo('ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux.GHGprofile.pt',\
    ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux)
    >>> print gas_code
    ['Press', 'Temp', 7, 1, 6, 27, 45, 2, 3, 5, 25, 34, 22]
    >>> print molwgt
    28.4540589019
    >>> print filename
    atm/ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux.atm
        
    """

    #----importing necessary subroutines------#
    import numpy as np
    from gas_info import gas_info
    gases = gas_info()
    import pgrid
    #-----------------------------------------#

    #----------------reading photochemical input file ----------------------------------------#
    # This will read data into a tuple 
    input = np.genfromtxt(infile,dtype=None)
    #-----------------------------------------------------------------------------------------#


    #------------convert data into an indexed, trackable, interpolated, sorted array---------#
    #NOTE: Functionality replaces profile.f, pgrid.f, xyinterp.f

    # pressure index
    iP = [y for y in input[0]].index('Press')
    #pressure array - converts to float, flattens into 1D array, and converts to pascal
    in_P_arr = np.array(map(float,input[1:,iP])).flatten()*scaleP
    # temperature index
    iT = [y for y in input[0]].index('Temp')
    # temperature array
    in_T_arr = np.array(map(float,input[1:,iT])).flatten()

    #list of matches between gases file and input file so input can be arbitrary number of gases
    Formula = ['Press','Temp']
    gas_code = ['Press','Temp']
    column_pos = [iP,iT] #original column position
    atm_pos = [1, 2] #new column positions that correspond to Formula/gas_code
    # gas counter for new column positions
    x = 3
    for i in range(len(input[0])):
        for j in range(len(gases['Formula'])):
            if (gases['Formula'][j] == input[0,i]):
                Formula.append(str(gases['Formula'][j]))
                gas_code.append(int(j))
                column_pos.append(int(i))
                atm_pos.append(int(x))
                x = x + 1

    #find max and min pressure levels
    Pmax = np.amax(in_P_arr)
    Pmin = np.amin(in_P_arr)
    #create new pressure grid - SMART maxes out at ~62 lines
    if plvls > 60:
         print 'plvls is greater than 60, SMART might not like that!'
    out_P_arr = pgrid.pgrid(plvls,Pmin,Pmax)
    #make an array containing pressure, temperature, and mixing ratios should be (nlevels by 2+Ngas)
    #ignores columns like density, altitude, etc. and gases not loaded in gas_info
    #adds N2 if condition keyword met
    if addn2:
        out_arr = np.zeros((plvls,len(column_pos)+1),dtype='float')
        Formula.append('N2')
        gas_code.append(22)
        column_pos.append(-1)
        atm_pos.append(int(x))
        x = x + 1
    else:
        out_arr = np.zeros((plvls,len(column_pos)),dtype='float')
    #fill pressure axis
    out_arr[:,0] = out_P_arr
    #interpolate and fill temperature axis
    #the [::-1] returns a 'view' of the reversed array
    out_arr[:,1] = np.interp(out_P_arr,in_P_arr[::-1],in_T_arr[::-1])
    #loop through the other gases and interpolate the mixing ratios
    for i in xrange(2,len(column_pos)):
        in_field_arr = np.array(map(float,input[1:,column_pos[i]])).flatten()
        out_field_arr = np.interp(out_P_arr,in_P_arr[::-1],in_field_arr[::-1])
        out_arr[:,i] = out_field_arr
    #fills N2 rmix array/column and calculates mean molecular weight of atmosphere
    molwgt = 0.
    for j in range(plvls):
        if addn2:    
            out_arr[j,-1] = 1.e0 - np.sum(out_arr[j,2:])
        for i in xrange(2,len(gas_code)):
            molwgt = molwgt + (gases['mass'][gas_code[i]]*out_arr[j,i])*out_arr[j,0]
    molwgt = molwgt/np.sum(out_arr[:,0])

    #-----------------------writing .atm file--------------------------------------#
    #name output .atm file
    atm_file = atm_dir + tag + '.atm'

    # - fill header string
    header = ''
    for i in range(len(Formula)):
        if i == 0:
            header = '{:^9}'.format(str(Formula[i]))+(' ')
        elif i == 1:
            header = header + '{:^11}'.format(str(Formula[i]))+(' ')
        else: 
            header = (header + '{:^11}'.format(str(Formula[i])+
                     '('+str(gas_code[i])+')')+(' '))

    #write output .atm file to text file
    np.savetxt(atm_file, out_arr, fmt='%0.5e', header = header)
    #------------------------------------------------------------------------------#     
    
    #------------return info necessary to write LBLABC/SMART scripts---------------#
    return gas_code, atm_pos, molwgt, atm_file
    #------------------------------------------------------------------------------#
