# -*- coding: iso-8859-1 -*-
def write_cld(tag, tau, alt, fractal=True, prefix='', 
              nalt = 55, h_cld_dir = '/astro/users/giada/photochem_smart/haze_io/cld/', header = 'alt(km)     mode'):

   """Writes a .cld file based on input optical depths from a lognormal fractal haze distribution

   This function writes .cld optical depth reference files for SMART. Specifically, input altitude dependent
   optical depths are taken as input and interpolate onto grid(s) that have few enough data points for SMART 
   to sample (< 62). The smaller dimension of the input optical depth array is assumed be equal to the number
   of particle modes that require .cld files and a new .cld file is written for each one with the altitude 
   abscissa present in alt. 

   Parameters
   ----------
   tag : string
       A unique indentifier used as a substring in all outputs
       Ex: 'Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux' 
   tau : float array
       A multidimensional numpy float array that contains calculated optical depths for each alt abscissa.  
       Dimensions: [# altitudes, # modes]
   alt : float array
       A one dimensional array of altitudes (in km) that the abscissa points for tau
       Size: # altitudes
   fractal : bool, optional
       A keyword flag that indicates the particles are fractals. This affects only the naming of output files.
       Default: 'fractal'
   prefix : string, optional
       A string that is appended to the name of output. cld files
       Default: none
   nalt : integer, optional
       An integer indicating how many altitude abscissa are written to the output file
       SMART maxes out at 62
       Default: 55
   h_cld_dir : string, optional
       Path to directory where output .cld files are written to
       Default: 'haze_io/cld/'
   header : string, optional
       Unit header for output file
       Default: 'alt(km)     mode'

    Returns
    ----------
    nmodes : integer
       Number of particle modes, which is also the number of dimensions in tau and the number of output files
       Ex : 19
    fnames : string list
       A list of strings with each element containing the path and name of each output .cld file

        Notes
    ----------
    This function writes .cld optical depth reference files for SMART.

    Revision History
    ----------
    Written by E. Schwieterman July, 2014
    Edited by G. Arney August, 2014


    Examples    
    ----------   

   """
   #---------import necessary packages-----------------#
   import numpy as np
   import pdb
   #---------------------------------------------------#
   #pdb.set_trace()

   #---------interpolate input altitudes and taus to new grid -----------------------#
   #make an array equally spaced in altitude with nalt steps
   alt_nalt = np.linspace(alt.min(0), alt.max(0), nalt)
   #make a multidimensional tau array with the appropriate number of dimensions
   #np.shape(0) = number of rows of tau, nalt number of elements for each tau array
   tau_nalt = np.zeros((nalt,tau[0,:].shape[0]))
   # interpolate tau values to new grids
   for j in range(tau[0,:].shape[0]): tau_nalt[:,j] = np.interp(alt_nalt, alt, tau[:,j])

    # number of particle modes
   nmodes = tau[0,:].shape[0]
    # number of altitude abscissa points
   nalt = nalt # redundant
   
    #clean mode arrays
   for i in range (nmodes):
      temparr = tau_nalt[:,i]
      # eliminate small tau values
      ismall = np.where(temparr < 1.e-20)
      temparr[ismall] = 0.0
      # don't think you need this
      tau_nalt[:,i] = temparr

   #---------------------------------------------------------------------------------#

   #-----------general header info---------------------------------------------#
   h0 = '\n'
   h1 = 'Model Atmosphere Aerosol Distribution: Mode'
   h2 = '\n'
   h3 = 'Number of aerosol layers = ' + str(nalt) + '\n'
   h4 = 'Aerosol reference wavenumber = 10000 (1.0 um)\n'
   h5 = '\n'
   h6 = 'Homogenous layer aerosol optical depths\n'
   h7 = '\n'
   #----------------------------------------------------------------------------#

   #-------------------name and write to output .cld files----------------------#
   #list of file names
   fnames = []
   fnames_nodir = []
   #loop through all modes and write to file
   for k in range(nmodes):
       #name output file
       if fractal:
          fname = h_cld_dir + tag + prefix + 'FRACTAL_MODE_' + str(k) + '.cld'
          fname_nodir = tag + prefix + 'FRACTAL_MODE_' + str(k) + '.cld'
       if not fractal:
          fname = h_cld_dir + tag + prefix + '_MODE_' + str(k) + '.cld'
          fname_nodir = tag + prefix + '_MODE_' + str(k) + '.cld'
       #keep track of file names in list
       fnames.append(fname)
       fnames_nodir.append(fname_nodir)
       #generate header for file
       h1 = 'Model Atmosphere Aerosol Distribution: Mode ' + str(k)
       h = h0 + h1 + h2 + h3 + h4 + h5 + h6 + h7 + header
       #shape and create output array for two column output
       out_array = np.zeros((nalt,2), dtype=float).reshape((55,2))
       out_array[:,0] = np.reshape(alt_nalt, (55))
       out_array[:,1] = np.reshape(tau_nalt[:,k], (55))
       #save to text
       np.savetxt(fname, out_array, delimiter='     ', fmt = ['%5.1f', '%4.2e'], header = h, comments='')      
       #----------------------------------------------------------------------------#

   #return number of modes and list of file names
   return nmodes, fnames_nodir
