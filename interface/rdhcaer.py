# -*- coding: iso-8859-1 -*-
def rdhcaer(infile, hcaer_dir = '~/photochem_smart/haze_io/hcaer/', skipline = 3, fractally=True):

   """reads standard hcear*.out file and returns standard parameters

   This function reads and parses a standard hcaer*.out file from the photochemical model.
   The function returns the parameters in the hcaer*.out file as numpy arrays.

    Parameters
    ----------
    infile : string
        hcaer photochemical model output file. Should have one header row at line 4.
        Ex: 'hcaer2.5_Ga_1.0E-02_ch4_rmix_1.013_bar.out'
    hcaer_dir : string, optional
        directory that contains the hcaer*.out files
        Note: currently assumed to be contained within the infile string
        Default: 'haze_io/hcaer/'
    skipline : integer, optional
        Number of lines to skip at top of file
        Default : 3

    Returns
    ---------- 
    Z_arr : float array
       altitudes in centimeters
    aerosol_arr : float array
       number density in particles/cm^3
    RPAR_arr :  float array
       radius of equivalent mass spherical particles in cm
    RFRAC_arr : float array
       radius of fractal in cm (the code actually needs RPAR, defined above)
    WFALL_arr : float array
       fall velocity cm/s
    TAUSED_arr : float array
       e folding timescale against sedimentation (s)
    TAUEDD_arr : float array
       e folding Eddy diffusion timescale (s)
    TAUC_arr : float array
       e folding lifetime against coagulation (s)
    CONVER_arr : float array
       stupidly named conversion factor that is actually the number of molecules per particle
       (the joys of photochemical code variable names)

    Notes
    ---------- 
    Reads standard hcear*.out file and returns standard parameters. 
    Z_arr, aerosol_arr, RPAR_arr, and RFRAC_arr are likely to be used by haze_photo_smart.py 

    Revision History
    ---------- 
    Written by E. Schwieterman and G. Arney July, 2014

    Examples   
    ---------- 
    >>> infile = 'hcaer2.5_Ga_1.0E-02_ch4_rmix_1.013_bar.out'
    >>> zz, aerosol, rpar, rfrac, wfall, taused, tauedd, tauc, conver = rdhcaer(infile)
    >>> print zz
    [   25000.   125000.   225000.   325000.   425000.   525000.   625000.\
       725000.   825000.   925000.  1025000.  1125000.  1225000.  1325000.\
      1425000.  1525000.  1625000.  1725000.  1825000.  1925000.  2025000.\
      2125000.  2225000.  2325000.  2425000.  2525000.  2625000.  2725000.\
      2825000.  2925000.  3025000.  3125000.  3225000.  3325000.  3425000.\
      3525000.  3625000.  3725000.  3825000.  3925000.  4025000.  4125000.\
      4225000.  4325000.  4425000.  4525000.  4625000.  4725000.  4825000.\
      4925000.  5025000.  5125000.  5225000.  5325000.  5425000.  5525000.\
      5625000.  5725000.  5825000.  5925000.  6025000.  6125000.  6225000.\
      6325000.  6425000.  6525000.  6625000.  6725000.  6825000.  6925000.\
      7025000.  7125000.  7225000.  7325000.  7425000.  7525000.  7625000.\
      7725000.  7825000.  7925000.  8025000.  8125000.  8225000.  8325000.\
      8425000.  8525000.  8625000.  8725000.  8825000.  8925000.  9025000.\
      9125000.  9225000.  9325000.  9425000.  9525000.  9625000.  9725000.\
      9825000.  9925000.]


   """

   #----importing necessary subroutines------#
   import numpy as np
   #-----------------------------------------#

   #----------------reading photochemical input file ----------------------------------------#
   # This will read data into a tuple 
   input = np.genfromtxt(infile,dtype=None, skip_header = skipline)
   #-----------------------------------------------------------------------------------------#

   #----------convert into one dimensional float arrays--------------------------------------#
   # Z index
   iZ = [y for y in input[0]].index('Z')
   #Z array - converts to float, flattens into 1D array
   Z_arr = np.array(map(float,input[1:,iZ])).flatten()
   # aerosol index
   iaerosol = [y for y in input[0]].index('AERSOL')
   #aerosol array - converts to float, flattens into 1D array
   aerosol_arr = np.array(map(float,input[1:,iaerosol])).flatten()
   # rpar index
   if fractally: iRPAR = [y for y in input[0]].index('RPAR')
   #rpar array - converts to float, flattens into 1D array
   if not fractally: iRPAR = [y for y in input[0]].index('RPARs')
   #rpar array - converts to float, flattens into 1D array
   RPAR_arr = np.array(map(float,input[1:,iRPAR])).flatten()
   # rfrac index
   if fractally: iRFRAC = [y for y in input[0]].index('RFRAC')
   #rfrac array - converts to float, flattens into 1D array
   if fractally: RFRAC_arr = np.array(map(float,input[1:,iRFRAC])).flatten()
   if not fractally: RFRAC_arr = RPAR_arr
   # wfall index
   iWFALL = [y for y in input[0]].index('WFALL')
   #wfall array - converts to float, flattens into 1D array
   WFALL_arr = np.array(map(float,input[1:,iWFALL])).flatten()
   # taused index
   iTAUSED = [y for y in input[0]].index('TAUSED')
   #tased array - converts to float, flattens into 1D array
   TAUSED_arr = np.array(map(float,input[1:,iTAUSED])).flatten()
   # tauedd index
   iTAUEDD = [y for y in input[0]].index('TAUEDD')
   #tauedd array - converts to float, flattens into 1D array
   TAUEDD_arr = np.array(map(float,input[1:,iTAUEDD])).flatten()
   # tauc index
   iTAUC = [y for y in input[0]].index('TAUC')
   #tauc array - converts to float, flattens into 1D array
   TAUC_arr = np.array(map(float,input[1:,iTAUC])).flatten()
   # conver index
   iCONVER = [y for y in input[0]].index('CONVER')
   #conver array - converts to float, flattens into 1D array
   CONVER_arr = np.array(map(float,input[1:,iCONVER])).flatten()
   #----------------------------------------------------------------------------------------#

   #return paramters
   return Z_arr, aerosol_arr, RPAR_arr, RFRAC_arr, WFALL_arr, TAUSED_arr, TAUEDD_arr, TAUC_arr, CONVER_arr

