# -*- coding: iso-8859-1 -*-

def haze_photo_smart(fname, forwardscatt = False, debuggy = 1, makeplots = True, doit=1, fractally=True):


    """

     Takes calculated density and radii values from the photochemical model
     and turns the spherical particle monomodal photochem distribution into
     a lognormal distribution.  For fractal particles, it keeps them as
     a monomodal distribution to enable us to use Eric Wolf's fractal
     monomodal scattering files.  Particles > 0.05um in radius are treated
     as fractals; < 0.05um are treated as spheres.

     The code goes through each altitude layer of the photochemical model
     and bins each radius into the two modes that bracket it. 
     It calculates the number density of the two modes such that the 
     mass density of the layer is conserved as Dave Crisp instructed.
     Once the number density is known, optical depth of each layer can
     be calculated and passed into write_cld to generate cloud scripts
     that SMART can read.

     There are 18 modes total.  Need to edit param.inc for SMART and set
     nmodes to 18 or larger in order to use this code (SMART usually
     only handles 10 particle modes)

     if you find a bug please email giada.arney@gmail.com or giada@uw.edu
     
  
     Parameters
     ----------
     debuggy : integer
      set equal to 1 to print extra output
      set equal to 0 to suppress extra output

     forwardscatt : bool, optional
      set True incorporate forward scattering correction into tau calculation
      set False to not incorporate forward scattering correction into tau calculation

     yy : integer
      for loop variable for looping through hcaer files

     fname : array of strings
      Filenames of hcaer files we want to read in

     zz : float array
      altitudes from photochemical model

     aerosol : float array
      read in as number density but gets converted to mass density g/cm^3 of the aerosols

     rpar : float array
      radii of equivalent mass spherical particles [cm]
      turns out these (not rfrac) are the relevant radii because
      the radii in Eric Wolf's fractal files are actually the 
      equivl. mass sphere particles, not the fractal radii themselves

     rfrac : float array
      radii of fractal particles (see note above for rpar)

     wfall : float array
      read in for completeness; fall velocity [cm/s]

     taused : float array
      read in for completeness; timescale against sedimentation [s]

     tauedd : float array
      read in for completeness; timescale against eddy diffusion [s]

     tauc : float array
      read in for completenss; timescale against coagulation [s]

     conver : float array
      read in for completness; number of molecules per particle 

     width : float array
      width of each layer in photochemical model [cm]

     rmodes : float array
      radii of our particle modes that we're binning into. [defined as um but then converted to cm for use]

     nummodes : integer
      number of particle modes

     numlayers : integer
      number of altitude layers

     tauarray : float array [numlayers, nummodes]
      array of optical depths for each mode at each altitude

     numarray : float array [numlayers, nummodes]
      array of number densities for each mode at each altitude

     s : float 
      dimensionless geometric standard deviation 
      literature says 1.5 is a reasonable value

     sigma : float
      defined as log(s)

     rho : float
      density of aerosol material [g/cm**3]
      Trainer et al 2006 says should be 0.64 for Archean Earth

     nindex : float
      real index of refraction for Tholins at 1um (Khare et al 1984)

     kindex : float
      imaginary index of refraction for Tholins at 1um (Khare et al 1984)

     index : complex
      (nindex + j*kindex)

     rmon : float
      radius of monomers [cm]
      0.05 um -> 5e-6 cm

     df : float array
      fractal dimension of each particle mode
      not used for calculations but listed here for reference

     nmon : float array
      number of monomers in each particle mode

     Qext_ : float array
      extinction efficiency for each particle mode at 1um

     w_ : float array
      single scattering albedo for each particle mode at 1um

     g_ : float array
      asymmetry parameter for each particle mode at 1um
     
     ka : float
      fractal projected area area constant (Koylu et al 1995)
     
     alpha : float
      fractal projected area exponent (Koylu et al 1995)

     xx : integer
      for loop variable for looping through number of atmospheric layers

     M : float
      mass of the layer [xx] we are on [g/cm3]
    
     rphot : float
      radius of the layer [xx] we are on [cm]

     z : float
      width of the layer [xx] we are on [cm]

     binloc_lower : integer
      array location of smaller radius bin in rmodes array

     binloc_upper : integer
      array location of larger radius bin in rmodes array

     r1 : integer
      radius of smaller mode of the layer

     r2 : integer
      radius of the larger mode of the layer

     r_lim1_1 or r_lim1_2: floats
      lower limit of integration for lognormal distribution for r1 and r2, respectively
      defined as r1 or r2/10. and don't change unless you ALSO change
      the mie and mom files created by miescat

     r_lim1_2 or r_lim2_2: floats
      upper limit of integration for lognormal distribution for r1 and r2, respectively
      defined as r1 or r2*10. and don't change unless you ALSO change
      the mie and mom files created by miescat

     spacing1 and spacing2 : floats
      stepsize for integration over radii [cm]
     
     radii1 and radii2: float arrays
      radii to integrate over for smaller and larger mode, respectively
     
     dist_to_top : float
      log space distance from rphot to the larger bin

     dist_to_lower : float
      log space distance from rphot to smaller bin

     Nfactor : float
      how we relate the number densities for the smaller and larger radius bins in each layer
      defined as Nfactor = dist_to_lower/dist_to_top

     inte1 and inte 2: float
      integration variable for smaller and larger radius, respectively

     N_r1 : float
      number density of r1 particles at each altitude [#/cm3]

     N_r2 : float
      number density of r2 particles at each altitude [#/cm3]

     tau1 : float
      optical depth of r1 particles at each altitude

     tau2 : float
      optical depth of r2 particles at each altitude

     totaltauarray : float array
      [numlayers, nummodes]
      array of total optical depths for both modes at each layer

     totalnumarray : float array
      [numlayers, nummodes]
      array of total number densities for both modes at each layer

     heights : float array
      array of heights in km instead of cm for plotting purposes


     Returns
     ----------
     Feeds optical depth array (tauarray) into write_cld.py to generate SMART cloud files from the photochemical model 
     Returns 'hcaerfiles', an array with a list of the hcaer cloud filenames

     Notes
     ---------- 
     Main code for aerosol calculations
     Previously called lognormal_photochem_fractal in the IDL version

     1um is the reference wavelength.  

     Important!! This generates 18 particle modes to encompass all the size bins we
     have from Eric Wolf that are relevant to the photochemical output.
     The default version of SMART handles 10 particle modes max.  Must
     change nmodes value in param.inc and recompile SMART to use this code.
     (the version of SMART included with this version of smart_interface should be able to 
     handle 50 modes max so should work with this)

     Revision History
     ---------- 
     Written by G. Arney August, 2014
     Adapted from IDL codes written by G. Arney (originally written Dec. 2013)

     """

    #---------------------------------------------------------
    #import necessary things
    import math
    import numpy
    import sys
    from numpy import loadtxt
    from readcol import readcol
    import log_funcs
    import rdhcaer
    import write_cld
    readdir = '/astro/users/giada/photochem_smart/haze_io/hcaer/'
    #debuggy = 1
    #forwardscatt = 1

    #-------------------------------------------------------


    #fname = numpy.loadtxt(readdir+'list_files.lst', dtype='a')

    #doit = 1
    if doit == 1:
    #the above line is just so I don't have to mess with all the 
    #indentations below after removing the for loop below... :/

    #for yy in range(0, fname.size): 

    #-------------------------------------------------------
        infile = readdir+fname        
        print 
       
        zz, aerosol, rpar, rfrac, wfall, taused, tauedd, tauc, conver = rdhcaer.rdhcaer(infile, fractally=fractally) #read in HCAER file
        #zz, aerosol, rpar, rfrac, wfall, taused, tauedd, tauc, conver = readcol(readdir+fname, skipline=4, twod=False)

        numlayers = zz.size
        width = numpy.zeros(numlayers)   #array for width of layers for calculating optical depth


    #----define values & arrays to be used:
        um_per_cm = 1.e4    
         #binned radii 
        if fractally: rmodes = [0.001, 0.005, 0.01, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0]
        #might change spherical bins:
        if not fractally: rmodes = [0.001, 0.005, 0.01, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0]
        #old if not fractally: rmodes = [0.001, 0.01, 0.1, 1.0,]
        rmodes = [x/um_per_cm for x in rmodes]   

        nummodes = len(rmodes)
        tauarray = numpy.zeros((numlayers, nummodes))  #to save optical depths in for the particle modes
        numarray = numpy.zeros((numlayers, nummodes))  #to save particle densities in for the particle modes
         #SYNTAX: numpy.zeros(shape, dtype=float) shape is e.g. (2,3) or 2 or whatever.  can also say dtype 

        g_per_kg = 1000.
        cm_per_m = 100.                           
        ug_per_g = 1.e6
        cm3_per_m3 = 1.e6

        s= 1.5                     #dimensionless geometric standard deviation.

        sigma = math.log(s)

        rho = 0.64                
                                    #Note: Trainer et al (2006) has
                                    #suggested 0.64 g/cm^3 for early earth
                                    #hazes (0.84 is tholins)

        aerosol = aerosol * (4./3.)*math.pi*rpar**3*rho 
        #aerosol: convert particles/cm**3 to g/cm**3


        nindex = 1.65              #tholin real index of refraction at 1um (Khare and Sagan 1984) (1um = arbitrary reference wavelength)
        kindex = 1e-3              #tholin imaginary index of refraction at 1 um 
        index = complex(nindex,kindex) 


        rmon = 0.05                #um (monomer radius as defined by Wolf and Toon 2010 code)
        rmon = rmon / um_per_cm

    #these are from the files Shawn got from Eric Wolf:
    #not everything shown here is used in the code, but this is a useful reference list
    #Qext, w, and g at 1 um
    #            [0         1          2        3         4         5         6         7         8         9         10        11        12        13        14        15        16        17        18] #modenumber
    #            [0.001000, 0.005000, 0.010000, 0.050000, 0.060000, 0.070000, 0.080000, 0.090000, 0.100000, 0.200000, 0.300000, 0.400000, 0.500000, 0.600000  0.700000  0.800000  0.900000  1.0000000 2.000000] #mode radius 
    #            [10,       14,       19,       23,       24,       25,       26,       27,       28,       29,       30,       31,       32,       33        34        35        36        37        38] # RT file 
        df =    [3.,       3.,       3.,       3.,       1.5,      1.5,      1.5,      1.5,      1.5,      1.6,      1.8,      2.0,      2.27,     2.37,     2.39,     2.39,     2.39,     2.4,      2.4] #frac dimension  
        nmon =  [1.,       1.,       1.,       1.,       1.72,     2.74,     4.09,     5.83,     8.0,      64.,      216.,     512.,     1000.,    1728.,    2744.,    4096.,    5832.,    8000.,    640000.]                      
        Qext_ = [0.000013, 0.000065, 0.000135, 0.004208, 0.007665, 0.012491, 0.018590, 0.024999, 0.032071, 0.122678, 0.298257, 0.755670, 1.493625, 2.308136, 2.896976, 3.499912, 4.111387, 4.726953, 10.672200] 
        w_ =    [0.000043, 0.005309, 0.040891, 0.835328, 0.892116, 0.922760, 0.940348, 0.950203, 0.956792, 0.977408, 0.986062, 0.992665, 0.995361, 0.996398, 0.996652, 0.996832, 0.996966, 0.997068, 0.997403] 
        g_ =    [0.000008, 0.000211, 0.000842, 0.020874, 0.050540, 0.094012, 0.147557, 0.198785, 0.247661, 0.499368, 0.633979, 0.755536, 0.826912, 0.865900, 0.879201, 0.893083, 0.901904, 0.908937, 0.941749]

    #for fractal projected area:
        ka = 1.16                  #fractal area constant (Koylu et al 1995)
        alpha = 1.10               #fractal area exponent (Koylu et al 1995)

    #------------------------------------------------------
     #width of the layers:

        for q in range(0, numlayers-1): width[q] = zz[q+1] - zz[q]
        width[numlayers-1] = zz[numlayers-1] - zz[numlayers -2] #in cm

        for xx in range(0, numlayers):   
            M = aerosol[xx]         #g/cm**3 in layer      
            rphot = rpar[xx]        #already in cm
            z = width[xx]           #already in cm

    #----------------------------------------------------------------
    #DEAL WITH BINS ON EITHER SIDE OF RPHOT:

    #what if it falls directly ON a bin?
            bintest = rmodes == rphot
            whatisit = all(bintest == False)
            if whatisit == False: rphot = rphot * 1.0001
            #first line: fills an array with true/false depending on if there's a place where rmodes = rphot
            #second line: all looks to see if ALL elements of bintest are 'False'
            #third line: if whatisit is false, then not all elements of bintest are false, i.e. there's an element where 
            #that statement is true.

    #first thing is to determine which rmode bins rphot falls between

            gt=(rmodes>rphot).nonzero()
            lt=(rmodes<rphot).nonzero()
            binloc_lower = numpy.max(lt)
            binloc_upper = numpy.min(gt)

    #smaller radius : 
            r1 = rmodes[binloc_lower]
            r_lim1_1 = r1/10. #lower limit of integration for lognormal 
            r_lim2_1 = r1*10  #upper limit of integration for lognormal 
            spacing1 = r1/100.      #this can be played with. (stepsize for integration)
            radii1 = numpy.arange(r_lim1_1, r_lim2_1, spacing1) #SYNTAX HERE: (start, stop step)

    #larger radius :
            r2 = rmodes[binloc_upper]
            r_lim1_2 = r2/10.       #lower limit of integration for lognormal 
            r_lim2_2 = r2*10        #upper limit of integration for lognormal 
            spacing2 = r2/100.      #this can be played with. (stepsize for integration)
            radii2 = numpy.arange(r_lim1_2, r_lim2_2, spacing2)

    #distance between bins:
            dist_to_top = math.log10(r2) - math.log10(rphot)
            dist_to_lower = math.log10(rphot) - math.log10(r1)


    #here is how we will relate N1 and N2 (where N is the number density
    #of particles #/cm**3)
            Nfactor = dist_to_lower/dist_to_top # N2 = Nfactor*N1 

            if debuggy == 1:
                print 'rphot is: ' ;  print rphot
                print 'altitude is: ' ; print  zz[xx]
                print 'r1 is: ' ; print  r1
                print 'r2 is: ' ; print  r2
                print 'Nfactor is: ' ; print  Nfactor       

    #---------------------------------------------------------------
    #FIND N1 AND N2
            '''
            now we want to determine the total number of particles with
            characteristic radii r1 and r2  required 
            to get the same mass here as in the photochemical 
            The Wolf & Toon (2010) RT files we were given are 
            monomodal distributions.  Therefore, we do not use a lognormal
            distribution as we did for the spherical particle case EXCEPT where
            df = 3 (then the particle IS spherical)

            what we are doing:
            M = N1 * [integral(n(r1)) + Nfactor*integral(n(r2))]

            using trapezoidal rule for the integration
            '''        

            tot_inte1 = 0.
            tot_inte2 = 0.

    #to fix a weird issue:
            nradii1 = radii1.size
            nradii2 = radii2.size
            num = (nradii1, nradii2)
            minnum = numpy.min(num)


            if fractally and r2 <= 0.05/um_per_cm or fractally == False: #if both bins are in spherical regime

               for i in range(0, minnum-1):

                                    #integrate - somebody should take the time to optimize this
                  inte1 = spacing1 * (log_funcs.mass_r_b(radii1[i], r1, rho, sigma, 1.)+log_funcs.mass_r_b(radii1[i+1], r1, rho, sigma, 1.))/2.  #total mass
                  inte2 = spacing2 * (log_funcs.mass_r_b(radii2[i], r2, rho, sigma, Nfactor)+log_funcs.mass_r_b(radii2[i+1], r2, rho, sigma, Nfactor))/2. #total mass

                  tot_inte1 = tot_inte1 + inte1
                  tot_inte2 = tot_inte2 + inte2


            if fractally and r2 == 0.06/um_per_cm:  #if smaller bin is in spherical regime and larger in fractal

               for i in range(0, minnum-1):

                                    #integrate  - somebody should take the time to optimize this     

                   inte1 = spacing1 * (log_funcs.mass_r_b(radii1[i], r1, rho, sigma, 1.)+log_funcs.mass_r_b(radii1[i+1], r1, rho, sigma, 1.))/2. #total mass

                   tot_inte1 = tot_inte1 + inte1

               tot_inte2 = (4./3.)*math.pi*rho*Nfactor*rmodes[binloc_upper]**3. #total mass for r2 (monomodal)


            if fractally and r1 >= 0.06/um_per_cm:                                         #if both bins are in fractal regime
                tot_inte1 = (4./3.)*math.pi*rho* rmodes[binloc_lower]**3.        #total mass for r1 (monomodal)
                tot_inte2 = (4./3.)*math.pi*rho*Nfactor*rmodes[binloc_upper]**3. #total mass for r2 (monomodal)


            N_r1 = M/(tot_inte1+tot_inte2) # number density of r1 particles
            N_r2 = Nfactor * N_r1          # number density of r2 particles


    #----------------------------------------------------------
    # OPTICAL DEPTH:
            '''
            what we ultimately want for the cloud files is the total OPTICAL
            DEPTH in the atmospheric layer from each mode: 
            tau = (cross sectional area) * (number density) * (thickness of layer) * (wavelength-dependent extinction efficiency)

            For the fractal particles, we multiply the optical depth by a factor
            involving the asymmetry param and single-scattering albedo to produce
            the effective optical depth (Wolf and Toon 2010 supplemental
            material).  This can be turned on or off with the forwardscatt keyword.

            *******full explanation of tau for fractals: tau = qext * n * area************
            first term [1-w_[binloc_lower]/2. - (g_[binloc_lower]*w_[binloc_lower])/2.]
            is a correction for forward scattering used by Wolf and Toon (2010) provided
            in their supplementary information.  This term derives from Chelek and Wong (1995).
            Qext and n are extinction coeff and number density.
            The last part of the expression (rmon**2 * math.pi * (nmon[binloc_lower]/ka)**(1/alpha)) 
            gives the PROJECTED area of a fractal particle since it's clear when thinking of
            the shape of such a particle that you can't just use pi * rfract**2.  See Brasil et al 1999
            or Koylu et al 1995 for the expression N = ka*(Aa/Ap)**alpha.  Aa is the projected
            area of the aggregate while Ap is the area of a monomer given by pi * rmon**2.  Solving for Aa
            gives the expression here.  N is the number of monomers per fractal particle
            '''        
            tot_tau1 = 0.
            tot_tau2 = 0.


 
            if fractally and r2 <= 0.05/um_per_cm or fractally == False:#if both bins are in spherical regime
                for i in range(0, minnum-1):
   

                                    #integrate  - somebody should take the time to optimize this
                    inte1 = spacing1 * (log_funcs.area_r_b(radii1[i], r1, rho, sigma, N_r1, index)+log_funcs.area_r_b(radii1[i+1], r1, rho, sigma, N_r1, index))/2.
                    inte2 = spacing2 * (log_funcs.area_r_b(radii2[i], r2, rho, sigma, N_r2, index)+log_funcs.area_r_b(radii2[i+1], r2, rho, sigma, N_r2, index))/2.


                    tot_tau1 = tot_tau1 + inte1
                    tot_tau2 = tot_tau2 + inte2




            if fractally and r2 == 0.06/um_per_cm: #if smaller bin is in spherical regime and larger in fractal
   
                for i in range(0, minnum-1):

                                    #integrate  - somebody should take the time to optimize this
                  inte1 = spacing1 * (log_funcs.area_r_b(radii1[i], r1, rho, sigma, N_r1, index)+log_funcs.area_r_b(radii1[i+1], r1, rho, sigma, N_r1, index))/2.

                  tot_tau1 = tot_tau1 + inte1

                if forwardscatt: 
                    tot_tau2 = (1-w_[binloc_upper]/2. - (g_[binloc_upper]*w_[binloc_upper])/2.) * Qext_[binloc_upper] * N_r2 * rmon**2 * math.pi * (nmon[binloc_upper]/ka)**(1/alpha) 
                if not forwardscatt: 
                    tot_tau2 = Qext_[binloc_upper] * N_r2 * rmon**2 * math.pi * (nmon[binloc_upper]/ka)**(1/alpha)  
                    print 'not forward scatt'


            if fractally and r1 >= 0.06/um_per_cm:     

                if forwardscatt: 
                    tot_tau1 = (1-w_[binloc_lower]/2. - (g_[binloc_lower]*w_[binloc_lower])/2.) * Qext_[binloc_lower] * N_r1 * rmon**2 * math.pi * (nmon[binloc_lower]/ka)**(1/alpha) 
                    tot_tau2 = (1-w_[binloc_upper]/2. - (g_[binloc_upper]*w_[binloc_upper])/2.) * Qext_[binloc_upper] * N_r2 * rmon**2 * math.pi * (nmon[binloc_upper]/ka)**(1/alpha) 
                if not forwardscatt:              
                    tot_tau1 =  Qext_[binloc_lower] * N_r1 * rmon**2 * math.pi * (nmon[binloc_lower]/ka)**(1/alpha) 
                    tot_tau2 =  Qext_[binloc_upper] * N_r2 * rmon**2 * math.pi * (nmon[binloc_upper]/ka)**(1/alpha)    
                    print 'not forward scatt'



            tau1 = tot_tau1 * z
            tau2 = tot_tau2 * z

    #now save to our tauarray and numarray:
            tauarray[xx, binloc_lower] = tau1
            tauarray[xx, binloc_upper] = tau2

            numarray[xx, binloc_lower] = N_r1
            numarray[xx, binloc_upper] = N_r2

            if debuggy == 1:
                print 'tau 1 equals ' +str(tau1)
                print 'tau 2 equals ' +str(tau2)
                print 'N_r1 equals ' +str(N_r1)
                print 'N_r2 equals ' +str(N_r2)


            print  'end of aerosol layer number ' + str(xx)

    #total tau array:
            totaltauarray = numpy.zeros(zz.size)
            totalnumarray = numpy.zeros(zz.size)
            for mm in range(0, len(rmodes)): totaltauarray = totaltauarray + tauarray[:,mm] #+ tauarray2[0:-1,mm]       
            for mm in range(0, len(rmodes)): totalnumarray = totalnumarray + numarray[:,mm] #+ numarray2[0:-1,mm]

    #------------------------------------------------------
    #CLOUD FILE

    #last step is to write a cloud file:
        heights = zz / 1e5
        if fractally: nummodes, hcaerfiles = write_cld.write_cld(fname, tauarray, heights, fractal=True, prefix='', nalt = 55, h_cld_dir = 'haze_io/cld/', header = 'alt(km)     mode')
        if not fractally: nummodes, hcaerfiles = write_cld.write_cld(fname, tauarray, heights, fractal=False, prefix='', nalt = 55, h_cld_dir = 'haze_io/cld/', header = 'alt(km)     mode')

    #PLOTS!
    #-------make pretty plots!----------------------------
        if makeplots:
            from hazeplots import hazeplots
            if fractally: hazeplots(fname, numarray, tauarray, heights, aerosol, rpar, totaltauarray, totalnumarray, fractal=True)
            if not fractally: hazeplots(fname, numarray, tauarray, heights, aerosol, rpar, totaltauarray, totalnumarray, fractal=False) 

    #---------return hcaerfiles----------------------------

        return hcaerfiles
    
