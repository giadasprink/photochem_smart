# -*- coding: iso-8859-1 -*-
#file: rdsmartin

def rdhazecldin(filename = 'haze_io/hazecldin.txt'):

    """Reads input values for write_smart
     
    This function reads input values used by write_smart
    to generature runscripts for SMART. SMART will produce 
    an output file that contains the top-of-atmosphere spectral
    irradiance and other wavelength-dependent fluxes (depending on 
    the settings in the runsmart script). 

    Parameters
    ----------
    filename : string, optional
        path and name of input values for write_smart
        Default: 'haze_io/hazecldin.txt'

    Returns
    ----------
    hcaer : string, optional
        Do you want to include hydrocarbon aerosols?
        Default: No
    cirrus : string, optional
        Do you want to include cirrus clouds?
        Default: No
    stratocum : string, optional
        Do you want to include strato cumulus clouds?
        Default: No
    hcaertaudir : string, optional
        Directory for hydrocarbon aerosol optical depth (tau) files
    hcaermiedir : string, optional
        Directory for hydrocarbon aerosol mie and mom files
    cldtaudir : string, optional
        Directory for water cloud optical depth (tau) files
    cldmiedir : string, optional
        Directory for water cloud mie and mom files

    Revision History
    ----------
    Based on rdsmartin.py by E. Schwieterman
    Modified for hazes and clouds by G. Arney August, 2014

    Examples
    ----------
    >>>hcaer, cirrus, stratocum, hcaertaudir, hcaermiedir, \
    cldtausir, cldmiedir = rdhazecldin(filename = 'haze_io/hazecldin_default.txt')

    """

    # --------import numpy-----------------------------------#
    import numpy as np
    #--------------------------------------------------------#

    #-----generate from text---------------------------------#
    input = np.genfromtxt(filename,dtype=None,comments='#')
    #--------------------------------------------------------#

    #-----------name variables and correct type--------------#
    hcaer =           str(input[0])
    cirrus =          str(input[1])
    stratocum =       str(input[2])
    hcaertaudir =     str(input[3])
    hcaermiedir =     str(input[4])
    cldtaudir =       str(input[5])
    cldmiedir =       str(input[6])
    #--------------------------------------------------------#

    #----------------return values---------------------------#
    return hcaer, cirrus, stratocum, hcaertaudir, hcaermiedir, \
    cldtaudir, cldmiedir
    #--------------------------------------------------------#
