
def pgrid(nlev,pmin,pmax):

    """Creates Pressure Grid
   
     Generates a pressure grid that is equally spaced in
     the log of pressure

     Parameters
     -----------
     nlev : integer
         Number of pressure levels
         Ex: 60
     pmin : float
         Minimum pressure at highest level
         Ex: 1.e-6
     pmax : float
         Maximum pressure at lowest level
         Ex: 1.0
    
     Returns
     -----------
     p : array_like
         1D array nlev in size that contains
         pressure equally spaced in log pressure
 
     Notes
     -----------
     Replaces pgrid.f by David Crisp
 
     Revision History
     -----------
     Written by E. Schwieterman February, 2014
  
     Examples
     -----------
     >>> p = pgrid.pgrid(50,1.e-6,1.0)
     >>> print p
     [  1.00000000e-06   1.32571137e-06   1.75751062e-06   2.32995181e-06
        3.08884360e-06   4.09491506e-06   5.42867544e-06   7.19685673e-06
        9.54095476e-06   1.26485522e-05   1.67683294e-05   2.22299648e-05
        2.94705170e-05   3.90693994e-05   5.17947468e-05   6.86648845e-05
        9.10298178e-05   1.20679264e-04   1.59985872e-04   2.12095089e-04
        2.81176870e-04   3.72759372e-04   4.94171336e-04   6.55128557e-04
        8.68511374e-04   1.15139540e-03   1.52641797e-03   2.02358965e-03
        2.68269580e-03   3.55648031e-03   4.71486636e-03   6.25055193e-03
        8.28642773e-03   1.09854114e-02   1.45634848e-02   1.93069773e-02
        2.55954792e-02   3.39322177e-02   4.49843267e-02   5.96362332e-02
        7.90604321e-02   1.04811313e-01   1.38949549e-01   1.84206997e-01
        2.44205309e-01   3.23745754e-01   4.29193426e-01   5.68986603e-01
        7.54312006e-01   1.00000000e+00]

     """

     #import numpy
    import numpy as np

    # declare new pressure array
    p = np.zeros(nlev, dtype='float')
    # set up  
    p[0] = 0.
    nlevm = nlev - 1
    p[nlevm] = pmax
    if (pmin == 0.):
        pmin = 1.e-6
    alnp0 = np.log(pmin)
    alnp1 = np.log(pmax)
    dlnp = (alnp1 - alnp0)/(nlevm)
    # fill pressure grid
    for i in range(1, nlev):
        p[nlevm - i] = p[nlevm]*np.exp(-(np.log(p[nlevm]/p[nlevm-i+1])+dlnp))
    return p
