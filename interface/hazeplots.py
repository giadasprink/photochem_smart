def hazeplots(fname, numarray, tauarray, heights, aerosol, rpar, totaltauarray, totalnumarray, fractal=True):

    """
    Make various plots showing information from haze_photo_smart

    Parameters
    ---------
    All already defined in haze_photo_smart

    Returns
    ---------
    Plots! (obviously ;)

    Notes
    --------
    Plots are saved in haze_io/plots

    Revision History
    ---------
    Written by G. Arney August 2014
    Based on parts of IDL codes written by G. Arney in Dec. 2013
    """

       #pdb.set_trace()
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import *

    plt.clf()
    plot(numarray[:,0],heights, 'DarkGray',  label='0.001 $\mu$m')
    plot(numarray[:,1],heights, 'Black',  label='0.005 $\mu$m')
    plot(numarray[:,2],heights, 'Indigo',label='0.01 $\mu$m')
    plot(numarray[:,3],heights, 'Blue',label='0.05 $\mu$m')
    plot(numarray[:,4],heights, 'SteelBlue',label='0.06 $\mu$m')
    plot(numarray[:,5],heights, 'MediumAquaMarine',label='0.07 $\mu$m')
    plot(numarray[:,6],heights, 'MediumSeaGreen',label='0.08 $\mu$m')
    plot(numarray[:,7],heights, 'ForestGreen',label='0.09 $\mu$m')
    plot(numarray[:,8],heights, 'LimeGreen',label='0.1 $\mu$m')
    plot(numarray[:,9],heights, 'Chartreuse',label='0.2 $\mu$m')
    plot(numarray[:,10],heights, 'Gold',label='0.3 $\mu$m')
    plot(numarray[:,11],heights, 'Orange',label='0.4 $\mu$m')
    plot(numarray[:,12],heights, 'SandyBrown',label='0.5 $\mu$m')
    plot(numarray[:,13],heights, 'Chocolate',label='0.6 $\mu$m')
    plot(numarray[:,14],heights, 'SaddleBrown',label='0.7 $\mu$m')
    plot(numarray[:,15],heights, 'Maroon',label='0.8 $\mu$m')
    plot(numarray[:,16],heights, 'FireBrick',label='0.9 $\mu$m')
    plot(numarray[:,17],heights, 'Crimson',label='1.0 $\mu$m')
    plot(numarray[:,18],heights, 'HotPink', label='2.0 $\mu$m')
    plot(totalnumarray, heights, 'LightCoral', ls='--', label='Total')
    plt.xscale('log')
    plt.ylabel('altitude [km]')
    plt.xlabel('number density #/$cm^3$')
    plt.title('number density for '+fname, fontsize=7)
    legend(fontsize='x-small', loc='lower left')

    #plt.show()

    if not fractal: plotdir = 'haze_io/plots/'+fname+'_spherical_'
    if fractal: plotdir = 'haze_io/plots/'+fname
    #save it
    plt.savefig(plotdir+'_number_density.eps')
    plt.clf()

    plt.clf()
    plot(tauarray[:,0],heights, 'DarkGray',  label='0.001 $\mu$m')
    plot(tauarray[:,1],heights, 'Black',  label='0.005 $\mu$m')
    plot(tauarray[:,2],heights, 'Indigo',label='0.01 $\mu$m')
    plot(tauarray[:,3],heights, 'Blue',label='0.05 $\mu$m')
    plot(tauarray[:,4],heights, 'SteelBlue',label='0.06 $\mu$m')
    plot(tauarray[:,5],heights, 'MediumAquaMarine',label='0.07 $\mu$m')
    plot(tauarray[:,6],heights, 'MediumSeaGreen',label='0.08 $\mu$m')
    plot(tauarray[:,7],heights, 'ForestGreen',label='0.09 $\mu$m')
    plot(tauarray[:,8],heights, 'LimeGreen',label='0.1 $\mu$m')
    plot(tauarray[:,9],heights, 'Chartreuse',label='0.2 $\mu$m')
    plot(tauarray[:,10],heights, 'Gold',label='0.3 $\mu$m')
    plot(tauarray[:,11],heights, 'Orange',label='0.4 $\mu$m')
    plot(tauarray[:,12],heights, 'SandyBrown',label='0.5 $\mu$m')
    plot(tauarray[:,13],heights, 'Chocolate',label='0.6 $\mu$m')
    plot(tauarray[:,14],heights, 'SaddleBrown',label='0.7 $\mu$m')
    plot(tauarray[:,15],heights, 'Maroon',label='0.8 $\mu$m')
    plot(tauarray[:,16],heights, 'FireBrick',label='0.9 $\mu$m')
    plot(tauarray[:,17],heights, 'Crimson',label='1.0 $\mu$m')
    plot(tauarray[:,18],heights, 'HotPink', label='2.0 $\mu$m')
    plot(totaltauarray, heights, 'LightCoral', ls='--', label='Total')
    plt.xscale('log')
    plt.ylabel('altitude [km]')
    plt.xlabel('optical depth')
    plt.title('optical depth for '+fname, fontsize=7)
    legend(fontsize='x-small', loc='lower left')

    #plt.show()

    plotdir = 'haze_io/plots/'+fname
    #save it
    plt.savefig(plotdir+'_optical_depth.eps')
    plt.clf()

    f, (ax1, ax2) = plt.subplots(1,2, sharey=True)
    ax1.plot(aerosol, heights, color='HotPink', linewidth=2.5)
    ax1.set_xlabel('g/$cm^3$')
    ax1.set_title('mass density')
    ax1.set_ylabel('altitude [km]')

    ax2.plot(rpar*1e4, heights, color='MediumSeaGreen', linewidth=2.5)
    ax2.set_xlabel('particle radius [$\mu$m]')
    ax2.set_title('particle radii')  
    ax2.locator_params(nbins=4, axis='x')

    plotdir = 'haze_io/plots/'+fname
    #save it
    plt.savefig(plotdir+'_aerosol_rpar.eps')
    plt.clf()
