c FILE: PGRID.F
      SUBROUTINE PGRID(nlev,pmin,pmax,p)
C
C     CREATES A PRESSURE GRID THAT IS EQUALLY-SPACED IN
C     THE LOG OF PRESSURE
C
      INTEGER nlev
      REAL*8 nlevm
      REAL*8 pmin
      REAL*8 pmax
      REAL*8 p(nlev)
      REAL*8 alnp0
      REAL*8 alnp1
      REAL*8 dlnp
Cf2py intent(in) nlev, pmin, pmax
Cf2py intent(out) p
Cf2py depend(nlev) p
      p(1) = 0.D0
      nlevm = nlev-1
      p(nlev) = pmax
      if (pmin .eq. 0.D0) pmin = 1.D-6
      alnp0 = log(pmin)
      alnp1 = log(pmax)
      dlnp = (alnp1 - alnp0)/(nlevm)
      DO l = 1, (nlev-1)
        p(nlev-l) = p(nlev)*exp(-(log(p(nlev)/p(nlev-l+1))+dlnp))
      ENDDO
      END
C END FILE PGRID.F
