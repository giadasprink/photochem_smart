# -*- coding: iso-8859-1 -*-
#file: write_lblabc

def write_lblabc(tag, atm_file, gas_index, col_rmix, molwgt = 29.,
                 minwn = 300, maxwn = 100000, grav = 9.8,
                 radius = 6371., col_p = 1, col_t = 2, 
                 scaleP = 1.0, n_t_prof = 3, t_offset = 25.,
                 n_broad = 0, scale_rmix = 1.0, maxwidth = 1000.,
                 min_tau = 1.0e-5, out_dir = 'lblabc_io/output/',
                 script_dir = 'lblabc_io/runlblabc/',skip = 1, rmix_type = 1, 
                 par_file = 'fixed_input/HITRAN2012.par', hitran_tag = 'hitran2012',
                 fundamntl_file = 'fixed_input/fundamntl.dat', shortfname=True):

    """Writes a runlblabc script

    This function writes to a text file that contains ordered input for LBLABC.
    LBLABC will calculate the line-by-line absorption coefficients for an input
    gas whose mixing ratios (and pressure-temperature profile) are known. 
    
    Parameters
    ----------
    tag : string
        A unique indentifier used as a substring in all outputs
        Ex: 'Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux'     
    atm_file : string
        Name and path to the .atm file used by LBLABC.
        Ex: 'atm/Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux.atm'
    gas_index : integer
        HITRAN gas index for gas whose .abs coefficients are being
        calculated by LBLABC
        Ex: 7 (O2)
    col_rmix : integer
        Column of .atm file that contains the mixing ratio for the gas.
        Ex: 3
    molwgt : float, optional
        Mean molecular weight of the atmosphere (g/mol)
        Default: 29.
    minwn : integer, optional
        Minimum wavenumber cutoff for LBLABC
        Default: 300 (33.3 um)
    maxwn : integer, optional
        Maximum wavenumber cutoff for LBLABC
        Default: 100000 (0.1 um)   
    grav : float, optional
        Gravitational acceleration (m/s2)
        Default: 9.8
    radius : float, optional
        Planetary radius (km)
        Default: 6371.
    col_p : integer, optional
        Column of input file containing pressure values
        Default: 1
    col_t : integer, optional
        Column of input file containing temperature values
        Default: 2
    scaleP : float, optional
        Pressure scaling factor used by LBLABC
        Default: 1.0
    n_t_prof : integer, optional
        Number of temperature profiles to calculate .abs coefficients for
        Default: 3
    t_offset : float, optional
        Temperature profile offset (K)
        Default: 25.0
    n_broad : integer, optional
        Number of foreign broadening gases
        Default: 0
    scale_rmix : float, optional
        Scaling factor for mixing ratio
        Default: 1.0
    max_width : float, optional
       Maximum line width
       Default: 1000.0
    min_tau : float, optional
       Minimum column optical depth
       Default: 1.e-5
    out_dir : string, optional
       Directory to put output .abs file (BIG!)
       Default: 'lblabc_io/output/'
    script_dir : string, optional
       Directory to write the runlblabc script to
       Default: 'lblabc_io/runlblabc/'
    skip : integer, optional
       Number of lines LBLABC should skip at top of .atm file
       Default: 1
    rmix_type : integer, optional
       Type of mixing ratio - volume (1) or mass (2)
       Default: 1
    par_file : string, optional
       Path and filename of HITRAN parameter file (.par)
       Default: 'fixed_input/hitran08.par'
    fundamntl_file : string, optional
       Path and filename of fundamntl file (.dat)
       Default: 'fixed_input/fundamntl.dat'

    Returns
    ----------
    script_file : string
        Path and name of file containing LBLABC script
    output_file : string
        Path and name of file containing .abs script after LBLABC runs

    Notes
    ----------
    Writes runlblabc script to designated script directory

    Revision History
    ----------
    Written by E. Schwieterman January, 2014

    Examples
    ----------
    >>> script_file, lblabc_file  = write_lblabc('Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux',\
    '/astro/users/eschwiet/modeling/photochem_smart/atm/Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux.atm',\
     7, 3, molwgt = 28.5)
    >>> print script_file
    lblabc_io/runlblabc/runlblabc_Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux_o2_hitran2012_300_100000cm.scr
    >>> print lblabc_file
    lblabc_io/output/Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux_o2_hitran2012_300_100000cm.abs

    """

    #------------import necessary packages---------------------------------------#
    from gas_info import gas_info
    gases = gas_info()
    #----------------------------------------------------------------------------#

    #-------------name script and output files-----------------------------------#
    #name script file
    script_file = (script_dir + 'runlblabc' + '_' + tag + '_' + 
                  str(gases['formula'][gas_index]) + '_' + 
                  hitran_tag + '_' + str(int(minwn)) + '_' +
                   str((int(maxwn))) + 'cm.scr')

    #name output .abs file
    if shortfname == False: output_file = (out_dir + tag + '_'+ str(gases['formula'][gas_index]) 
                                           + '_' + hitran_tag + '_' + str(int(minwn)) + '_' +
                                           str((int(maxwn))) + 'cm.abs')

    if shortfname: output_file = (out_dir + tag + '_'+str(gases['formula'][gas_index])+'_.abs')

    #----------------------------------------------------------------------------#

    #-------------writing runlblac script----------------------------------------#
    
    #open output file for writing/appending
    out = open(script_file, 'w')
    #write to file 
    #list directed for all runs
    out.write('{:<40}'.format('3'))
    out.write('format list directed\n')
    #atm file with p,t columns
    out.write(atm_file+'\n')
    #records to skip at top of file
    out.write('{:<40}'.format(str(skip)))
    out.write('# records to skip at TOF\n')
    #columns of pressure, temperature
    out.write('{:<40}'.format(str(col_p)+','+str(col_t)))
    out.write('columns of p and t\n')
    #pressure scaling factor
    out.write('{:<40}'.format(str(scaleP)))
    out.write('pressure scaling factor\n')
    #number of temperature profiles
    out.write('{:<40}'.format(str(n_t_prof)))
    out.write('number of t profiles\n')
    #temperature profile offset
#    out.write('{:<40}'.format(str(t_offset)))
#    out.write('temperature profile offset (K)\n')
    #hitran gas index
    out.write('{:<40}'.format(str(gas_index)))
    out.write('hitran gas index\n')
    #foreign broadening gases - hardcoded to zero for now (EWS Jan 2014)
    out.write('{:<40}'.format('0'))
    out.write('# of foreign broadening gases\n')
    #format (list directed always)
    out.write('{:<40}'.format('3'))
    out.write('format (list directed)\n')
    #atm file with p,rmix columns
    out.write(atm_file+'\n')
    #records to skip at top of file
    out.write('{:<40}'.format(str(skip)))
    out.write('# records to skip at TOF\n')
    #pressure coordinate
    out.write('{:<40}'.format(str(col_p)))
    out.write('vert coordinate (pres)\n')
    #columns of p and rmix
    out.write('{:<40}'.format(str(col_p)+','+str(col_rmix)))
    out.write('columns of p and rmix\n')
    #mixing ratio type (1 = volume (default), 2 = mass)
    out.write('{:<40}'.format(str(rmix_type)))
    out.write('Volume(1)/Mass(2) Mixing Ratio\n') #need to fix this
    #write molecular mass if Volume Rmix
    #p, rmix scaling factors
    out.write('{:<40}'.format(str(scaleP)+','+str(scale_rmix)))
    out.write('p, rmix scaling factors\n')
    # read hitran formatted file
    if str(rmix_type) == str(1):
        out.write('{:<40}'.format(str(gases['mass'][gas_index])))
        out.write('Molecular weight of gas in g/mol\n')
    out.write('{:<40}'.format('2'))
    out.write('read hitran 08 formatted file\n')
    #location/name of HITRAN parfile
    out.write(par_file+'\n')
    #gravity
    out.write('{:<40}'.format(str(grav)))
    out.write('gravitational acceleration\n')
    #planet radius
    out.write('{:<40}'.format(str(radius)))
    out.write('planet radius\n')
    #mol. wgt. of atmosphere (kg/km)
    out.write('{:<40}'.format(str(molwgt)))
    out.write('mol. wgt. of atmosphere (kg/km)\n')
    #min, max wavenumbers
    out.write('{:<40}'.format(str(minwn)+','+str(maxwn)))
    out.write('min, max wavenumber\n')
    #maximum line width
    out.write('{:<40}'.format(str(maxwidth)))
    out.write('maximum line width\n')
    #minimum column optical depth
    out.write('{:<40}'.format(str(min_tau)))
    out.write('minimum column optical depth\n')
    #write fundamntl directory and file
    out.write(fundamntl_file+'\n')
    #write abs coefficient output file path and name
    out.write(output_file+'\n')
    #overwrite previous file
    out.write('{:<40}'.format('2'))
    out.write('Overwrite\n')
    #----------------------------------------------------------------------------#

    #return script name and output file name
    return script_file, output_file
