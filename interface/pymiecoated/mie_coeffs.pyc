ó
Qc           @   s   d  Z  d d l m Z m Z m Z m Z m Z m Z m Z d d l	 m
 Z
 m Z d e f d     YZ d   Z d   Z d   Z d	 S(
   s(  
Copyright (C) 2012-2013 Jussi Leinonen

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
iÿÿÿÿ(   t   pit   aranget   zerost   hstackt   sqrtt   sint   cos(   t   jvt   yvt	   MieCoeffsc           B   s   e  Z d  Z d   Z RS(   s&   Wrapper for the Mie coefficients.
    c         C   s"   t  |  \ |  _ |  _ |  _ d  S(   N(   t
   mie_coeffst   ant   bnt   nmax(   t   selft   par(    (    s   pymiecoated/mie_coeffs.pyt   __init__   s    (   t   __name__t
   __module__t   __doc__R   (    (    (    s   pymiecoated/mie_coeffs.pyR	      s   c         C   s¿  |  d d k	 r  t |  d  n d } |  d d k	 rF t |  d  n d } | d k sd | d k rs t d   n  |  d d k	 r t |  d  n	 t d  } |  d d k	 r¿ t |  d  n d } |  d d k	 rå t |  d  n d } | d k	 } | | d k k rt d   n  | rC| t d  k rCt d	 d
   n  | sX| } | } n  | | k sp| | k rt | | |  } n6 | d k r¦t | | |  } n t | | | |  } | S(   sF   Input validation and function selection for the Mie coefficients.
    t   epst   xs#   Must specify x and either eps or m.t   mug      ð?t   yt   eps2s0   Must specify both y and m2 for coated particles.s/   Multilayer calculations for magnetic particles s   are not currently supported.i    N(   t   Nonet   complext   floatt
   ValueErrort   single_mie_coefft   coated_mie_coeff(   t   paramsR   R   R   R   R   t   coatedt   coeffs(    (    s   pymiecoated/mie_coeffs.pyR
   !   s,    &&,&&	c         C   s  t  |  |  | } t  |  |  } t t d | d | d d   } | d } t t t | t |   d   } t |  } | d }	 t  d t |  }
 |
 t |	 |  } t t	 |  | |  f  } |
 t
 |	 |  } t t |  | |  f  } | t d	 d  | } | t d	 d  | } t | d
 t } xJ t | d d	 d  D]2 } | d | } | d | | | | | d <qYW| |  } | d } | | | | } | | | | } | | | | | | } | | | | | | } | | | f S(   sV  Mie coefficients for the single-layered sphere.

    Args:
        eps: The complex relative permittivity.
        mu: The complex relative permeability.
        x: The size parameter.

    Returns:
        A tuple containing (an, bn, nmax) where an and bn are the Mie
        coefficients and nmax is the maximum number of coefficients.
    i   i   g      ð?g      @i   i   g      ø?g      à?i    t   dtypeiÿÿÿÿ(   R   t   intt   roundt   maxt   absR   R    R   R   R   R   R   R   R   t   xrange(   R   R   R   t   zt   mR   t   nmax1t   nmxt   nt   nut   sxt   pxt   p1xt   chxt   ch1xt   gsxt   gs1xt   dnxt   jt   rt   dnt   n1t   dat   dbR   R   (    (    s   pymiecoated/mie_coeffs.pyR   G   s2    &
%
"

c   9      C   sÆ  t  |   } t  |  } | | } | | } | | } | | }	 t t d | d | d d   }
 t t | |  t |	   } t t t |
 |  d   } |
 d } t |
  } t |
 d t } t |
 d t } t |
 d t } t | d t } x t | | |	 f | | | f  D]d \ } } xJ t	 | d d d	  D]2 } | d | } | d | | | | | d <qGW| |
  | (q$W| d
 } | |	 | g } g  | D] } t  d t
 |  ^ q¬} g  t | |  D] \ } } | t | |  ^ qÜ\ } } } g  t | |  D]  \ } } | t | |  ^ q\ } }  }! t t |  | |  f  }" t t |  |! |  f  }# | t d d  |! }$ |" t d d  |# }% | | | }& | | | }' | | }( | |  }) |& |( | }* |' |( | }+ | |  |( }, | | | }- |& |, |- }. |' |, |- }/ |* |. }0 |+ |/ }1 |0 | }2 |1 | }3 | d | }4 |2 | |4 }5 | |3 |4 }6 | |5 |" |$ |5 |% }7 | |6 |" |$ |6 |% }8 |7 |8 |
 f S(   sÂ  Mie coefficients for the dual-layered (coated) sphere.

       Args:
          eps: The complex relative permittivity of the core.
          eps2: The complex relative permittivity of the shell.
          x: The size parameter of the core.
          y: The size parameter of the shell.

       Returns:
          A tuple containing (an, bn, nmax) where an and bn are the Mie
          coefficients and nmax is the maximum number of coefficients.
    i   i   g      ð?g      @i   i   R"   i    iÿÿÿÿg      ø?g      à?(   R   R#   R$   R%   R&   R   R   R   t   zipR'   R    R   R   R   R   R   (9   t   eps1R   R   R   t   m1t   m2R)   t   ut   vt   wR   t   mxR+   R*   R,   t   dnut   dnvt   dnwR5   R(   R8   R6   R7   R-   t   vwyt   xxR.   t   st   pvt   pwt   pyt   chvt   chwt   chyt   p1yt   ch1yt   gsyt   gs1yt   uut   vvt   fvt   fwt   ku1t   kv1t   ptt   pratt   ku2t   kv2t   dns1t   gns1t   dnst   gnst   nratt   a1t   b1R   R   (    (    s   pymiecoated/mie_coeffs.pyR   s   sb    



&
."
'>?





N(   R   t   numpyR    R   R   R   R   R   R   t   scipy.specialR   R   t   objectR	   R
   R   R   (    (    (    s   pymiecoated/mie_coeffs.pyt   <module>   s   4	&	,