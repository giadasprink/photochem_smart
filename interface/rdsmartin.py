# -*- coding: iso-8859-1 -*-
#file: rdsmartin

def rdsmartin(filename = 'smart_io/smartin.txt'):

    """Reads input values for write_smart
     
    This function reads input values used by write_smart
    to generature runscripts for SMART. SMART will produce 
    an output file that contains the top-of-atmosphere spectral
    irradiance and other wavelength-dependent fluxes (depending on 
    the settings in the runsmart script). 

    Parameters
    ----------
    filename : string, optional
        path and name of input values for write_smart
        Default: 'smart_io/smartin.txt'

    Returns
    ----------
    t_surf : float
        Surface temperature input for SMART (in kelvins)
        Default: 288.0
    atm_p_col : integer, optional
        Column in atm_file that contains the pressure abscissa
        Default: 1
    atm_t_col : integer, optional
        Column in the atm file that contains the temperature 
        (matched to atm_p_col)         
        Default: 2
    scaleP : float, optional
        Pressure scaling factor used by SMART
        Default: 1.0
    minwn : integer, optional
        Minimum wavenumber cutoff for SMART
        Default: 300 (33.3 um)
    maxwn : integer, optional
        Maximum wavenumber cutoff for SMART
        Default: 100000 (0.1 um)
    grav : float, optional
        Gravitational acceleration (m/s2)
        Default: 9.8
    radius : float, optional
        Planetary radius (km)
        Default: 6371.    
    dmolwgt : float, optional
        Mean molecular weight of the atmosphere (g/mol)
        Default: 29.
    atm_skip : integer, optional
       Number of lines SMART should skip at top of .atm file
       Default: 1
    alb_file : string, optional
       Name and path to the albedo file used by SMART
       Default: 'fixed_input/albedo/seawater.alb'
    alb_skip : integer, optional
       Number of lines to skip at the top of the albedo file
       Default: 27
    r_AU : float, optional
       Distance from start in Astronomical Units (AU)
       Default: 1.0
    rmix_type : integer, optional
       Mixing ratio type - 1) Volume, 2) Mass
       Default: 1
    nstream: integer, optional
       Number of flux streams
       Default: 8
    source: integer, optional
       Contributions to source function - 
       Thermal (1), Solar (2), Both (3)
       Default: 3
    scale_rmix : float, optional
        Scaling factor for mixing ratio
        Default: 1.0
    spec : string, optional
        Name and path to stellar spectrum
        Default: 'fixed_input/specs/Kurucz1cm-1_susim_atlas2.dat'
    spec_skip : integer, optional
        Number of lines to skip at top of spectrum file
        Default: 12
    spec_unit : integer, optional
        Stellar spectrum units -
        1) - [W/m*m/cm], 2) - [W/m*m/nm], 3) - [W/m*m/um]
        Default: 1
    specx : integer, optional
        Stellar spectrum abscissa type - 
        1) - wavelength, 2) - wavenumber
        Default: 2
    specx_scale : float, optional
        Adjust stellar spectrum abscissa
        Default: 1.0
    spec_wn_col : integer, optional
        Column number of spectrum file with wavelength/wavenumber
        Default: 1
    spec_flux_col : integer, optional
        Column number of spectrum file with spectral flux
        Default: 2
    n_sza : integer, optional
        Number of solar zenith angles
        Default: 1
    za : float, optional
        Solar zenith angle
        Default: 60.0
    aa : float, optional
        Azimuth angle
        Default: 0.0
    con_crit : float, optional
        Convergence criterion
        Default: 0.01
    out_format : integer, optional 
        Output Format
        Default: 1
    n_aa : integer, optional
        Number of azimuth angles
        Default: 1
    aa2 : float, optional
        Azimuth angle
        Default: 0.0
    out_level : integer, optional
        level of output -
        1 - TOA
        2 - surface
        3 - TOA and surface
    out_unit : integer, optional
        Output Units - 
        1 - [W/m*m/sr/wavenumber]
        2 - [W/m*m/sr/micron]
        3 - [W/m*m/sr/nm]
        4 - [W/m*m/sr/Angstrom]
        Default: 2
    grid_type : integer, optional
        2 - slit
        Default: 2
    response : integer, optional
        Response function
        2 - Triangular
        Default: 2
    FWHM : float, optional
        Full-width at half max
        Default: 1.0
    sample_res : float, optional
        Sampling resolution [per cm^-1]
        Default: 1.0
    err_tau : float, optional
        Error for tau binning
        Default: 0.25
    err_pi0 : float, optional
        Error for pi binning
        Default: 0.15
    err_g : float, optional
        g Error
        Default: 0.15
    err_alb : float, optional
        Albedo error
        Default: 0.02
    out_format2 : integer, optional
        Output format 
        1 - ascii, 2 - binary
        Default: 1
    script_dir : string, optional
        Directory where runsmart scripts reside
        Default: 'smart_io/runsmart/'
    hitran_tag : string, optional
        Database tag for script names and output strings
        Default: hitran2012
    out_dir : string, optional
        Output directory for SMART runs
        Default: 'smart_io/output/'
    xsec_dir : string, optional
        Directory where cross section files reside
        Default: 'fixed_input/xsec/'
    abs_dir : string, optional
        Directory to look for .abs absorption coefficient files
        Default: 'lblabc_io/output/'
    atm_dir : string, optional
        Directory to look for .atm files
        Default: 'atm/'
    hcaer : string, optional
        Do you want to include hydrocarbon aerosols?
        Default: No
    cirrus : string, optional
        Do you want to include cirrus clouds?
        Default: No
    stratocum : string, optional
        Do you want to include strato cumulus clouds?
        Default: No

    Revision History
    ----------
    Written by E. Schwieterman February, 2014
    Examples only work for default values.
    02/24/2014 - Now reads atm directory from input file - EWS

    Examples
    ----------
    >>>t_surf, atm_p_col, atm_t_col, scaleP, minwn, maxwn, grav, radius,\
    dmolwgt, atm_skip, alb_file, alb_skip, r_AU, rmix_type, nstream, source,\
    scale_rmix, spec, spec_skip, spec_unit, specx, specx_scale, spec_wn_col,\
    spec_flux_col, n_sza, za, aa, con_crit, out_format, n_aa, aa2, out_unit, \
    grid_type, response, FWHM, sample_res, err_tau, err_pi, err_g, err_alb, \
    out_format2, script_dir, hitran_tag, out_dir, xsec_dir, abs_dir,\
    atm_dir = rdsmartin(filename = 'smart_io/smartin_default.txt')
    >>>print n_sza
    1
    >>>print za
    60.0
    >>>print xsec_dir
    fixed_input/xsec

    """

    # --------import numpy-----------------------------------#
    import numpy as np
    #--------------------------------------------------------#

    #-----generate from text---------------------------------#
    input = np.genfromtxt(filename,dtype=None,comments='#')
    #--------------------------------------------------------#

    #-----------name variables and correct type--------------#
    t_surf =          float(input[0])
    atm_p_col =       int(input[1])
    atm_t_col =       int(input[2])
    scaleP =          float(input[3])
    minwn =           float(input[4])
    maxwn =           float(input[5])
    grav =            float(input[6])
    radius =          float(input[7])
    dmolwgt =         float(input[8])
    atm_skip =        int(input[9])
    alb_file =        str(input[10])
    alb_skip =        str(input[11])
    r_AU =            float(input[12])
    rmix_type =       str(input[13])
    nstream =         int(input[14])
    source =          int(input[15])
    scale_rmix =      float(input[16])
    spec =            str(input[17])
    spec_skip =       int(input[18])
    spec_unit =       int(input[19])
    specx =           int(input[20])
    specx_scale =     float(input[21])
    spec_wn_col =     int(input[22])
    spec_flux_col =   int(input[23])
    n_sza =           int(input[24])
    za =              float(input[25])
    aa =              float(input[26])
    con_crit =        float(input[27])
    out_format =      int(input[28])
    n_aa =            int(input[29])
    aa2 =             float(input[30])
    out_level =       int(input[31])
    out_unit =        int(input[32])
    grid_type =       int(input[33])
    response =        int(input[34])
    FWHM =            float(input[35])
    sample_res =      float(input[36])
    err_tau =         float(input[37])
    err_pi0 =         float(input[38])
    err_g =           float(input[39])
    err_alb =         float(input[40])
    out_format2 =     int(input[41])
    script_dir =      str(input[42])
    hitran_tag =      str(input[43])
    out_dir =         str(input[44])
    xsec_dir =        str(input[45])
    abs_dir =         str(input[46])
    atm_dir =         str(input[47])
    #--------------------------------------------------------#

    #----------------return values---------------------------#
    return t_surf, atm_p_col, atm_t_col, scaleP, minwn, maxwn, \
    grav, radius, dmolwgt, atm_skip, alb_file, alb_skip, r_AU, \
    rmix_type, nstream, source,scale_rmix, spec, spec_skip, \
    spec_unit, specx, specx_scale, spec_wn_col, spec_flux_col, \
    n_sza, za, aa, con_crit, out_format, n_aa, aa2, out_level, out_unit, \
    grid_type, response, FWHM, sample_res, err_tau, err_pi0, \
    err_g, err_alb, out_format2, script_dir, hitran_tag, out_dir, \
    xsec_dir, abs_dir, atm_dir
    #--------------------------------------------------------#
