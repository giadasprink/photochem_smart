# -*- coding: iso-8859-1 -*-
#file: write_smart

def write_smart(tag, lbltag, atm_file, t_surf, gas_absorbers, gas_cols,
                atm_p_col = 1, atm_t_col = 2, scaleP = 1.0,
                minwn = 300, maxwn = 100000, grav = 9.8,
                radius = 6371., molwgt = 29., atm_skip = 1,
                alb_file = 'fixed_input/albedo/ocean.alb',
                alb_skip = 27, r_AU = 1.0, rmix_type = 1,
                nstream = 8, source = 3, scale_rmix = 1.0,
                spec = 'fixed_input/specs/Kurucz1cm-1_susim_atlas2.dat',
                spec_skip = 12, spec_unit = 1, specx = 2, specx_scale = 1.0, 
                spec_wn_col = 1, spec_flux_col = 2, n_sza = 1, za = 60., 
                aa = 00., con_crit = 0.01, out_format = 1, n_aa = 1,
                aa2 = 0.0, out_level = 1, out_unit = 2, grid_type = 2, response = 2,
                FWHM = 1.0, sample_res = 1.0, err_tau = 0.25, err_pi0 = 0.15,
                err_g = 0.15, err_alb = 0.02, out_format2 = 1,
                script_dir = 'smart_io/runsmart/', hitran_tag = 'hitran2012',
                out_dir = 'smart_io/output/', xsec_dir = 'fixed_input/xsec/',
                abs_dir = 'lblabc_io/output/', num_aero = 0.0, hcaer = 'No', 
                cirrus = 'No', stratocum = 'No', hcaertaudir = 'haze_io/cld/', 
                hcaermiedir = 'fixed_input/hcaer_mie/', cldtaudir = 'fixed_input/cld/', 
                cldmiedir = 'fixed_input/cld/', hcaerfiles='', hcaerfile = '', gencloud=True, 
                fractally=True, shortfname=True): 

    """Writes a runsmart script

    This function writes to a text file that contains ordered input for SMART.
    SMART will produce an output file that contains the top-of-atmosphere spectral
    irradiance and other wavelength-dependent fluxes (depending on the settings in
    the runsmart script). 

    Parameters
    ----------
    tag : string
        A unique indentifier used as a substring in all outputs
        Ex: 'Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux'     
    atm_file : string
        Name and path to the .atm file used by LBLABC
        Ex: 'atm/Sun_0.0SorgFlux_30000ppmCO2_1.0CH4flux.atm'
    t_surf : float
        Surface temperature input for SMART (in kelvins)
        Ex: 288.5
    gas_absorbers : list of integers
        List of gas codes from gas_info
        Ex: [7, 1, 6, 27, 45, 2, 3, 5, 25, 34, 22]
    gas_cols : list of integers
        List of column numbers in atm_file corresponding to gas codes above
        Ex: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    atm_p_col : integer, optional
        Column in atm_file that contains the pressure abscissa
        Default: 1
    atm_t_col : integer, optional
        Column in the atm file that contains the temperature (matched to atm_p_col)         
        Default: 2
    scaleP : float, optional
        Pressure scaling factor used by SMART
        Default: 1.0
    minwn : integer, optional
        Minimum wavenumber cutoff for SMART
        Default: 300 (33.3 um)
    maxwn : integer, optional
        Maximum wavenumber cutoff for SMART
        Default: 100000 (0.1 um)
    grav : float, optional
        Gravitational acceleration (m/s2)
        Default: 9.8
    radius : float, optional
        Planetary radius (km)
        Default: 6371.    
    molwgt : float, optional
        Mean molecular weight of the atmosphere (g/mol)
        Default: 29.
    atm_skip : integer, optional
       Number of lines SMART should skip at top of .atm file
       Default: 1
    alb_file : string, optional
       Name and path to the albedo file used by SMART
       Default: 'fixed_input/albedo/seawater.alb'
    alb_skip : integer, optional
       Number of lines to skip at the top of the albedo file
       Default: 27
    r_AU : float, optional
       Distance from start in Astronomical Units (AU)
       Default: 1.0
    rmix_type : integer, optional
       Mixing ratio type - 1) Volume, 2) Mass
       Default: 1
    nstream: integer, optional
       Number of flux streams
       Default: 8
    source: integer, optional
       Contributions to source function - 
       Thermal (1), Solar (2), Both (3)
       Default: 3
    scale_rmix : float, optional
        Scaling factor for mixing ratio
        Default: 1.0
    spec : string, optional
        Name and path to stellar spectrum
        Default: 'fixed_input/specs/Kurucz1cm-1_susim_atlas2.dat'
    spec_skip : integer, optional
        Number of lines to skip at top of spectrum file
        Default: 12
    spec_unit : integer, optional
        Stellar spectrum units -
        1) - [W/m*m/cm], 2) - [W/m*m/nm], 3) - [W/m*m/um]
        Default: 1
    specx : integer, optional
        Stellar spectrum abscissa type - 
        1) - wavelength, 2) - wavenumber
        Default: 2
    specx_scale : float, optional
        Adjust stellar spectrum abscissa
        Default: 1.0
    spec_wn_col : integer, optional
        Column number of spectrum file with wavelength/wavenumber
        Default: 1
    spec_flux_col : integer, optional
        Column number of spectrum file with spectral flux
        Default: 2
    n_sza : integer, optional
        Number of solar zenith angles
        Default: 1
    za : float, optional
        Solar zenith angle
        Default: 60.0
    aa : float, optional
        Azimuth angle
        Default: 0.0
    con_crit : float, optional
        Convergence criterion
        Default: 0.01
    out_format : integer, optional 
        Output Format
        Default: 1
    n_aa : integer, optional
        Number of azimuth angles
        Default: 1
    aa2 : float, optional
        Azimuth angle
        Default: 0.0
    out_level : integer, optional
        level of output -
        1 - TOA
        2 - surface
        3 - TOA and surface
    out_unit : integer, optional
        Output Units - 
        1 - [W/m*m/sr/wavenumber]
        2 - [W/m*m/sr/micron]
        3 - [W/m*m/sr/nm]
        4 - [W/m*m/sr/Angstrom]
        Default: 2
    grid_type : integer, optional
        2 - slit
        Default: 2
    response : integer, optional
        Response function
        2 - Triangular
        Default: 2
    FWHM : float, optional
        Full-width at half max
        Default: 1.0
    sample_res : float, optional
        Sampling resolution [per cm^-1]
        Default: 1.0
    err_tau : float, optional
        Error for tau binning
        Default: 0.25
    err_pi0 : float, optional
        Error for pi binning
        Default: 0.15
    err_g : float, optional
        g Error
        Default: 0.15
    err_alb : float, optional
        Albedo error
        Default: 0.02
    out_format2 : integer, optional
        Output format 
        1 - ascii, 2 - binary
        Default: 1
    script_dir : string, optional
        Directory where runsmart scripts reside
        Default: 'smart_io/runsmart/'
    hitran_tag : string, optional
        Database tag for script names and output strings
        Default: hitran2012
    out_dir : string, optional
        Output directory for SMART runs
        Default: 'smart_io/output/'
    xsec_dir : string, optional
        Directory where cross section files reside
        Default: 'fixed_input/xsec/'
    abs_dir : string, optional
        Directory to look for .abs absorption coefficient files
        Default: 'lblabc_io/output/'
    hcaertaudir : string, optinal
        Directory to look for hcaer optical depth files
    hcaermiedir : string, optional
        Directory to look for hcaer mie and mom files
    cldtaudir : string, optional
        Directory to look for water cloud optical depth files
    cldmiedir : string, optional
        Directory to look for water cloud mom and mie files
    hcaerfiles : array of strings, optional
        Array of filenames generated by haze_photo_smart
    hcaerfile : string, optional
        name of hcaer file if haze_photo_smart wasn't run
    gencloud: boolean, optional
        True of new cloud files were generated during this run


    Returns
    ----------
    script_file : string
        Path and name of file containing LBLABC script
    output_file : string
        Path and name of file containing .abs script after LBLABC runs

    Notes
    ----------
    Writes runsmart script to designated script directory

    Revision History
    ----------
    Written by E. Schwieterman January, 2014
    Haze and cloud stuff added by G. Arney August 2014


    Examples    
    ----------      
    """


    #---------import necessary packages-----------------#
    import numpy as np
    from gas_info import gas_info
    gases = gas_info()
    #---------------------------------------------------#

    #name script file
    script_file = (script_dir + 'runsmart' + '_' + tag + '_' + 
                  hitran_tag + '_' + str(int(minwn)) + '_' + 
                   str((int(maxwn))) + 'cm.scr')

    #name SMART output file\
    if shortfname == False: output_file = (out_dir + tag + '_' + hitran_tag + '_' + 
                                      str(int(minwn)) + '_' + str((int(maxwn))) +
                                      'cm')

    if shortfname: output_file = (out_dir + tag)


    #number of gas absorbers
    ngas = len(gas_absorbers)
    #loop through gases and 

    #open output file for writing/appending
    out = open(script_file, 'w')
    #write to file
    #list directed - no jacobians for all (might change later)
    out.write('{:<24}'.format('0'))
    out.write('No Pressure Jacobians\n')
    out.write('{:<24}'.format('0'))
    out.write('No Jacobians for Temperature\n')
    out.write('{:<24}'.format('3'))
    out.write('Formatted Atmospheric Structure File\n')
    out.write(atm_file+'\n')
    out.write('{:<24}'.format(str(atm_skip)))
    out.write('Lines to Skip at Top of File\n')
    out.write('{:<24}'.format(str(atm_p_col)+','+str(atm_t_col)))
    out.write('Columns of P,T\n')
    out.write('{:<24}'.format(str(scaleP)))
    out.write('Pressure Scaling Factor\n')
    out.write('{:<24}'.format(str(t_surf)))
    out.write('Surface Temperature\n')
    out.write('{:<24}'.format(str(ngas)))
    out.write('Number of Gas Absorbers\n')
    #loop over all gas absorbers
    for i in range(ngas):
        #HITRAN gas index for this absorber
        index = int(gas_absorbers[i])
        #string designation for this absorber (e.g., 'h2o')
        string = gases['formula'][index]
        #column of mixing ratio of gas in .atm file
        rmix_col = int(gas_cols[i])
        #begin writing info for absorber
        out.write('{:<24}'.format(str(index)))
        out.write('Gas Code for Absorber ' + 
                  str(i+1) + '(' + string + ')\n')
        out.write('{:<24}'.format('0'))
        out.write('No Jacobians for Absorber ' + str(i+1) +'\n')
        #HITRAN gases are line absorbers with .abs files
        #check to see if HITRAN database has absorption between minwn and maxwn
        #compatability issue only use HITRAN gases < index 32
        if (index <= 31 and index != 22 \
                       and gases['maxwn'][index] >= minwn \
                       and gases['minwn'][index] <= maxwn):
            if not shortfname: abs_file =  (abs_dir + lbltag + '_' + 
                                            string  + '_' + hitran_tag + '_' + 
                                            str(int(minwn)) + '_' + str((int(maxwn))) 
                                            + 'cm.abs')
            if shortfname: abs_file = (abs_dir + lbltag + '_'+ string +'_.abs')
            out.write('{:<24}'.format('2'))
            out.write('Number of Gas Coefficient Types\n')
            out.write('{:<24}'.format('1'))
            out.write('Line Absorbers\n')
            out.write(abs_file +'\n')
        #N2 CIA
        elif index == 22:
            cia_file = xsec_dir + 'n4.cia'
            out.write('{:<24}'.format('2'))
            out.write('Number of Gas Coefficient Types\n')
            out.write('{:<24}'.format('2'))
            out.write('Collisional Absorption\n')
            out.write(cia_file+'\n')   
        else:
            out.write('{:<24}'.format('1'))
            out.write('Number of Gas Coefficient Types\n')
        #always have cross sections
        out.write('{:<24}'.format('3'))
        out.write('Cross Sections\n')
        xsec_file = xsec_dir + string + 'xsec.dat'
        out.write(xsec_file+'\n')
        out.write('{:<24}'.format('3'))
        out.write('List Directed\n')
        out.write(atm_file+'\n')
        out.write('{:<24}'.format(str(atm_skip)))
        out.write('Lines to Skip at Top of File\n')
        out.write('{:<24}'.format(str(atm_p_col)))
        out.write('Pressure Coordinate\n')
        out.write('{:<24}'.format(str(atm_p_col)+','+str(rmix_col)))
        out.write('Columns of P and rMix\n')
        out.write('{:<24}'.format(str(rmix_type)))
        out.write('Volume(1)/Mass(2) Mixing Ratio\n')
        out.write('{:<24}'.format(str(scaleP)+','+str(scale_rmix)))
        out.write('p, rmix scaling factors\n')
        #end loop
    #Aersol loop
    
    out.write('{:<24}'.format(str(num_aero)))
    out.write('Number of Aerosols\n')
              
    if hcaer == 'Yes':   
        if fractally:
            filetag = ['0.001', '0.005', '0.01', '0.05','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38']        
            for aa in range(0,18):
                if aa < 4:
                #spherical files
                    out.write('{:<24}'.format('0'))
                    out.write('No Jacobians\n')         
                    out.write(hcaermiedir+'archean_tholin_'+filetag[aa]+'\n')
                    out.write('{:<24}'.format('18'))
                    out.write('Lines to Skip\n')
                    out.write('{:<24}'.format('1,7,8,11'))
                    out.write('Columns of wl,qext,qsca,and g1\n')
                    out.write('{:<24}'.format('1'))
                    out.write('Full Phase Function\n')
                    out.write('{:<24}'.format('17'))
                    out.write('Lines to Skip\n')
                    if gencloud: out.write(hcaertaudir+hcaerfiles[aa]+'\n')
                    if not gencloud: out.write(hcaertaudir+hcaerfile+'FRACTAL_MODE_'+str(aa)+'.cld\n')
                    out.write('{:<24}'.format('10000.'))
                    out.write('Standard wn for Optical Depth\n')
                    out.write('{:<24}'.format('9'))
                    out.write('Lines to Skip\n')
                    out.write('{:<24}'.format('2'))                	
                    out.write('Vertical Coordinate is Altitude\n')
                    out.write('{:<24}'.format('1,2'))              	
                    out.write('Columns of z,tau\n')
                    out.write('{:<24}'.format('1.,1.'))          	
                    out.write('Scaling Factors\n')
                    out.write('{:<24}'.format('false'))             	
                    out.write('Use Scale Heights\n')
                    
                if aa >= 4:
                #fractal files
                    out.write('{:<24}'.format('0'))
                    out.write('No Jacobians\n') 
                    out.write(hcaermiedir+'RT.SW.'+filetag[aa]+'.fractal.r50nm.khare\n')
                    out.write('{:<24}'.format('6'))                	
                    out.write('Lines to Skip\n')
                    out.write('{:<24}'.format('1,2,3,4'))          	
                    out.write('Colums of wl,qext,qsca,and g1\n')
                    out.write('{:<24}'.format('2'))                 	
                    out.write('Henyay-Greenstein\n')
                    if gencloud: out.write(hcaertaudir+hcaerfiles[aa]+'\n')
                    if not gencloud: out.write(hcaertaudir+hcaerfile+'FRACTAL_MODE_'+str(aa)+'.cld\n')
                    out.write('{:<24}'.format('10000.'))           	
                    out.write('Standard wn for Optical Depth\n')
                    out.write('{:<24}'.format('8'))                	
                    out.write('Lines to Skip\n')
                    out.write('{:<24}'.format('2'))                	
                    out.write('Vertical Coordinate is Altitude\n')
                    out.write('{:<24}'.format('1,2'))              	
                    out.write('Columns of z,tau\n')
                    out.write('{:<24}'.format('1.,1.'))          	
                    out.write('Scaling Factors\n')
                    out.write('{:<24}'.format('false'))             	
                    out.write('Use Scale Heights\n')
        
        
        if not fractally:
            filetag = ['0.001', '0.005', '0.01', '0.05', '0.06', '0.07', '0.08', '0.09', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0', '2.0']
            for aa in range(0,18):
                #all spherical files
                out.write('{:<24}'.format('0'))
                out.write('No Jacobians\n')         
                out.write(hcaermiedir+'archean_tholin_'+filetag[aa]+'\n')
                out.write('{:<24}'.format('18'))
                out.write('Lines to Skip\n')
                out.write('{:<24}'.format('1,7,8,11'))
                out.write('Columns of wl,qext,qsca,and g1\n')
                out.write('{:<24}'.format('1'))
                out.write('Full Phase Function\n')
                out.write('{:<24}'.format('17'))
                out.write('Lines to Skip\n')
                if gencloud: out.write(hcaertaudir+hcaerfiles[aa]+'\n')
                if not gencloud: out.write(hcaertaudir+hcaerfile+'_MODE_'+str(aa)+'.cld\n')
                out.write('{:<24}'.format('10000.'))
                out.write('Standard wn for Optical Depth\n')
                out.write('{:<24}'.format('9'))
                out.write('Lines to Skip\n')
                out.write('{:<24}'.format('2'))                	
                out.write('Vertical Coordinate is Altitude\n')
                out.write('{:<24}'.format('1,2'))              	
                out.write('Columns of z,tau\n')
                out.write('{:<24}'.format('1.,1.'))          	
                out.write('Scaling Factors\n')
                out.write('{:<24}'.format('false'))             	
                out.write('Use Scale Heights\n')
      

    if cirrus == 'Yes':
        out.write('{:<24}'.format('0'))                       
        out.write('No Jacobians\n')
        out.write(cldmiedir+'baum_cirrus_de100\n')
        out.write('{:<24}'.format('1'))                      
        out.write('Lines to Skip')
        out.write('{:<24}'.format('1,4,5,3'))               
        out.write('Columns of wl, qext, qsca, g1\n')
        out.write('{:<24}'.format('2'))
        out.write('HG Phase Function\n')
        out.write(cldtaudir+'cld_tau.dat\n')
        out.write('{:<24}'.format('15400.0'))                 
        out.write('Standard wn\n')
        out.write('{:<24}'.format('4'))                      
        out.write('Lines to Skip\n')
        out.write('1')                       
        out.write('Pressure Coordinate\n')
        out.write('{:<24}'.format('1,2'))                
        out.write('Columns of P and tau\n')
        out.write('{:<24}'.format('1.e5,2.'))                
        out.write('Convert to Pascals and tau\n')
        out.write('{:<24}'.format('false'))                   
        out.write('No Scale Heights\n')

    if stratocum == 'Yes':
        out.write('{:<24}'.format('0'))                       
        out.write('No Jacobians\n')
        out.write(cldmiedir+'strato_cum\n')
        out.write('{:<24}'.format('19'))                      
        out.write('Lines to Skip\n')
        out.write('{:<24}'.format('1,7,8,11'))                
        out.write('Columns of wl, qext, qsca, g1\n')
        out.write('{:<24}'.format('1'))                       
        out.write('Full Phase Function\n')
        out.write('{:<24}'.format('17'))                      
        out.write('Lines to Skip\n')
        out.write(cldtaudir+'cld_tau.dat\n')
        out.write('{:<24}'.format('15400.0'))                 
        out.write('Standard wn\n')
        out.write('{:<24}'.format('28'))                      
        out.write('Lines to Skip\n')
        out.write('{:<24}'.format('1'))                       
        out.write('Pressure Coordinate\n')
        out.write('{:<24}'.format('1,2'))                     
        out.write('Columns of P and tau\n')
        out.write('{:<24}'.format('1.e5,1.'))                
        out.write('Convert to Pascals and tau\n')
        out.write('{:<24}'.format('false'))                   
        out.write('No Scale Heights\n')

    #edit for more phase functions later?
    out.write('{:<24}'.format('0'))
    out.write('Lambertian\n')
    out.write('{:<24}'.format('0'))
    out.write('No Jacobians\n')
    out.write('{:<24}'.format('3'))
    out.write('List Direct\n')
    out.write(alb_file+'\n')
    out.write('{:<24}'.format(str(alb_skip)))
    out.write('Lines to Skip\n')
    #make these variables later?
    out.write('{:<24}'.format('1,2')) #
    out.write('Columns\n')
    out.write('{:<24}'.format('1')) #
    out.write('Wavelength Units\n')
    out.write('{:<24}'.format('1.')) #
    out.write('Convert to um\n')
    out.write('{:<24}'.format('1.'))#
    out.write('Albedo Scaling\n')
    out.write('{:<24}'.format(str(r_AU)))
    out.write('Distance from Star (AU)\n')
    out.write('{:<24}'.format(str(grav)))
    out.write('Gravitational Acceleration\n')
    out.write('{:<24}'.format(str(radius)))
    out.write('Planet Radius\n')
    out.write('{:<24}'.format(str(molwgt)))
    out.write('mol. wgt. of atmosphere (kg/km)\n')
    #rayleigh scatters hardcoded for now      
    out.write('{:<24}'.format('1'))
    out.write('Number of Rayleigh Scatters\n')
    out.write('{:<24}'.format('1'))
    out.write('Rayeligh Scatter Index\n')
    out.write('{:<24}'.format('1.0'))  
    out.write('Volume Mixing Ratio\n')
    out.write('{:<24}'.format(str(nstream)))
    out.write('Number of Streams\n')
    out.write('{:<24}'.format(str(source)))
    out.write('Source - Thermal(1), Solar(2), Both(3)\n')
    out.write('{:<24}'.format('3'))
    out.write('File Format\n')
    out.write(str(spec)+'\n')
    out.write('{:<24}'.format(str(spec_skip)))
    out.write('Lines to Skip\n')
    out.write('{:<24}'.format(str(spec_unit)))
    out.write('Units [W/m*m/(1-cm,2-um,3-nm)]\n')
    out.write('{:<24}'.format(str(specx)))
    out.write('Wave[length(1) [um]/number(2)] Abscissa\n')
    out.write('{:<24}'.format(str(specx_scale)))
    out.write('Abscissa scaling factor\n')
    out.write('{:<24}'.format(str(spec_wn_col)+','+str(spec_flux_col)))
    out.write('Columns of wn and Flux\n')
    out.write('{:<24}'.format(str(n_sza)))
    out.write('Number of Solar Zenith Angles\n')
    out.write('{:<24}'.format(str(za)+','+str(aa)))
    out.write('Zenith and Azimuth Angles\n')
    out.write('{:<24}'.format(str(con_crit)))
    out.write('Convergence Criterion\n')
    out.write('{:<24}'.format(str(out_format)))
    out.write('Output format\n')
    out.write('{:<24}'.format(str(n_aa)))
    out.write('Number of Azimuth Angles\n')
    out.write('{:<24}'.format(str(aa2)))
    out.write('Azimuth Angles\n')
    out.write('{:<24}'.format(str(out_level)))
    out.write('1 - TOA, 2 - surface, 3 - TOA and surface\n')
    out.write('{:<24}'.format(str(out_unit)))
    out.write('Output Radiance Units [W/m*m/sr/micron]\n')
    out.write('{:<24}'.format(str(minwn)+','+str(maxwn)))
    out.write('Min and Max Wavenumber\n')
    out.write('{:<24}'.format(str(grid_type)))
    out.write('Type of grid\n')
    out.write('{:<24}'.format(str(response)))
    out.write('Response Function - Triangular(2)\n')
    out.write('{:<24}'.format(FWHM))
    out.write('FWHM\n')
    out.write('{:<24}'.format(str(sample_res)))
    out.write('Sampling resolution [per cm]\n')
    out.write('{:<24}'.format(str(err_tau)))
    out.write('Error for tau Binning\n')
    out.write('{:<24}'.format(str(err_pi0)))
    out.write('Error for Pi0 Binning\n')
    out.write('{:<24}'.format(str(err_g)))
    out.write('g Error\n')
    out.write('{:<24}'.format(str(err_alb)))
    out.write('Albedo Error\n')
    out.write('{:<24}'.format(str(out_format2)))
    out.write('Output Format (ascii)\n')
    out.write(output_file+'\n')
    out.write('{:<24}'.format('2'))
    out.write('Overwrite\n')

    #return script file and output file names
    return script_file, output_file

             
       


#inputs - necessary
#tag
#atm filename/directory

#number of gas absorbers
#mixing ratio of atmosphere

#FOR EACH ABSORBER
#absorption coefficient file paths
#ames
#rmix column
#number of coefficient types
#cross section file paths
#ames
#albedo filename/path
#number of aerosols


#inputs - optional/fixed
#column p (1)
#column t (2)
#pressure scaling factor (1.0)
#rmix scaling factor for each gas
#surface temperature

#input in order-------------------
#0 No Pressure Jocobians
#0 No Jocaboians for Temperature
#atm file
#lines to skip at top of atm file
#columns of p,t in .atm file
#pressure scaling factor
#surface temperature
#number of gas absorbers
#--------------for each absorber (xNabsorber)-------------
    #gas code for absorber
    #no jacobians
    #number of gas coefficients types(xtypes)
        #----for each type-----------
        # type (1 - line absorber, 2 - CIA, 3 -cross section)
        # file to .abs or .dat or .cia
        #----------------------------
    #list directed
    #.atm file
    #lines to skip at TOF
    #pressure coodinate
    #columns of p, rmix
    #volume (1) or mass (2) mixing ratio
    #p, rmix scaling factors
#--------------------------------------------
#number of aerosols
#---------for each aerosol(x n aerosols)-----
    #number of aerosols
    #no jacobians
    #path and name of scattering file (no .mie or .mom)
    #lines to skip in cld file
    #columns of (wl, qext, qsca, g1)
    #phase function (1 - full)
    #lines to skip
    #cloud file
    #standard wavenumber
    #lines to skip
    #pressure coordinate
    #columsn of P and tau (1,2)
    #convert to pascals and tau (1.e5,1.)
    #false - no scale ehigts
#--------------------------------------------
#0 Lambertian
#3 List directed
#albedo file and path
#lines to skip in albedo file
#columns (e.g. 1,2)
#1 wavelength units (1 - micron)
#1. convert to um
#1. albedo scaling factor
#1. Distance from star (AU)
#9.80 gravity
#planetary radius
#mean molecular weight of atmosphere
#number of rayleigh scatters (default 1)
#1.0 volume mixing ratio
#8 number of streams
#3 Source - Thermal and solar (thermal - 1, solar -2)
#3 File format
#path to and name of stellar spectrum
#12 lines to skip in stellar spectrum file
#1 units [1 - [W/m*m/cm]
#Wavenumber Abscissa
#1,2 Columns of wn and Flux
#1 Number of Solar Zenith Angles
#0.,0. Zenith and Azimuth angles
#0.01 convergence riterion
#1 output format
#1 Number of Azimuth Angles
#0. Azimuth angles
#1 Top of Atmosphere only
#2 Output Radiance Units [W/m*m/sr/micron]
#300.,10000. Min and Max Wavenumber
#2 Type of Grid (slit)
#2 Triangular Spectral Response Functon
#1.0 FWHM
#1.0 Sampling Resolution [per cm]
#0.25 Error for tau Binning
#0.15 Error for pi0 Binning
#0.15 g error
#0.02 Albedo error
#1 Output format (ascii)
#NAME OF OUTPUT FILE
#2 Overwrite
