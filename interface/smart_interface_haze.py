# -*- coding: iso-8859-1 -*-
#file: smart_interface

def smart_interface_haze(photfile, tag, hcaerfile='', lblabc_exe = 'fixed_input/lblabc',
                    smart_exe = 'fixed_input/smart_spectra',
                    smart_out = 'smart_out.txt', lblabc_out = 'lblabc_out.txt',
                    lblin_file = 'lblabc_io/lblin.txt', smartin_file = 'smart_io/smartin.txt',
                    atm_dir = 'atm/',  hazecldin_file = 'haze_io/hazecldin.txt', 
                    lbl_in_default = False,  smart_in_default = False, 
                    genlblabc = True, runsmart = True, plvls = 60, gencloud = True,
                    hazecloud = True, haze_in_default = False, forwardscatt = False, makeplots = True, 
                    fractally = True, shortfname=True, difflbl=False, difflbltag=''):
    
    '''Interfaces with SMART

    Together with the imported modules this code will take a photochemical model output file read
    and parse the P-T structure and gas mixing ratios, generate a new .atm file with a new pressure
    grid equally spaced in logarithmic pressure space, interpolate the gas mixing ratios to this new
    pressure grid, write runlblabc scripts for each HITRAN gas, run LBLABC for each runlblabc script,
    generate a runsmart script (no aerosol scenario only for now), and run SMART. The user defined
    parameters for LBLABC and SMART are given in text files read by rdlblin and rdsmartin. Note the
    large number of keyword options there are for each submodule. 


    Parameters
    ----------
    photfile : string
        Photochemical model output file. Should have one header row.
        Ex: 'ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux.GHGprofile.pt'
    tag : string
        A unique indentifier used as a substring in all outputs
        Ex: 'ADLeo_30.0SorgFlux_30000ppmCO2_1.0CH4flux'
    hcaerfile : string
        Name of the hcaer file for the given photfile
        Don't need to include if gencloud = false
    lblabc_exe : string, optional
        Path to and name of compiled lblabc exe
        Default: 'fixed_input/lblabc'
    smart_exe : string, optional
        Path to and name of compiled smart exe
        Default: 'fixed_input/smart_spectra'
    smart_out : string, optional
        Name of file for stdout text from SMART when called
        Default: 'smart_out.txt' (current directory)
    lblabc_out : string, optional
        Name of fuke fir stdiyt text from LBLABC when called
        Default: 'lblabc_out.txt'
    lblin_file : string, optional
        Name of file containing user inputs for LBLABC
        Only used when lbl_in_default = False
        Default:  'lblabc_io/lblin.txt'
    smartin_file : string, optional
        Name of file containing user inputs for SMART
        Only used when smart_in_default = False
        Default: 'smart_io/smartin.txt'
    atm_dir : string, optional
        Directory to place new .atm file
        Default: 'atm/'
    lbl_in_default : bool, optional
        Toggle to set inputs to write_lblabc as defaults instead
        of reading lblin_file (instead this is ignored)
        See write_lblabc.py for default values
        Default: False
    smart_in_default : bool, optional
        Toggle to set input to write_smart as defaults instead
        of reading smart_in_file (instead this is ignored)
        See write_smart.py for default values
        Default: False
    genlblabc : bool, optional
        Toggle to run lblabc for each runlblabc script
        Default: True
    runsmart : bool, optional
        Toggle to run SMART for smart script generated
        Default: True
    plvls : integer, optional
        Number of pressure levels in new .atm file. SMARTS chokes >~62
        Default: 60 
    gencloud : bool, optional
        Toggle to generate new hcaer cloud files
    hazecloud : bool, optional
        A way to override the content of haze_io/hazecldin.txt from the
        program call.  If false will incorporate no hazes or clouds regardless
        of the content of hazecldin.txt (useful if running from a wrapper script
        and want to easily generate hazy files and corresponding nohaze files)
    haze_in_default : bool, optional
        Toggle to read input from default hazecldin file
    forwardscatt : bool, optional
        Toggle to incorporate forward scattering correction to cloud
        optical depth calculation (see Wolf and Toon (2010) supplementary
        information)
    makeplots : bool, optional
        Toggle to make plots displaying hydrocarbon haze information if
        generating hcaer new cloud files (default True)
    fractally : bool, optional
        Toggle whether particles are spheres or fractals (fractals if true)
    shortfname: bool, optional
        Toggle to make the filenames shorter (removes wn range info and hitran
        info in filenames...useful if 'tag' is long since smart has a char limit)
    difflbl: bool, optional
        have different tag for lblabc files than for smart files (useful
        in cases with hazes...haze smart files have 'HAZE' appended to their tag
        when running from run_many.py
        so will the .abs files, but that's silly since the lbl files for 
        haze and haze-free runs may be identical...you don't want to generate the same
        files under different filenames!
    difflbltag: string, optional
        write in tag for lbl files if difflbl is activated.  if running from the 
        'run_many' script, the default is to just remove the 'HAZE' part of the lbl tags.
    #not added yet!!!
    #writelblabc : bool, optional
        #Toggle to write runlblabc scripts
        #Can set to False if scripts already written
        #Default: True
    #writesmart: bool, optional
        #Toggle to write runsmart scripts
        #Can set to False if scripts already written
        #Default: True
    #not added yet!!!#
    
        
    Returns
    ----------
    None

    Functions Called
    ----------
    read_photo.read_photo
    rdlblin.rdlblin
    rdsmartin.rdsmartin
    rdhazecldin.rdhazecldin
    write_lblabc.write_lblabc
    haze_photo_smart.haze_photo_smart
    write_smart.write_smart
    gas_info.gas_info
    
    subprocess

    Revision History
    ----------
    Written by E. Schwieterman January-February, 2014
    02/24/14 - Modified code to allow change of .atm directory for runlblabc and runsmart
    scripts from directory the file is written to one given in input files - EWS
    08/15/14 - Modified code to incorporate hydrocarbon haze and water clouds - G. Arney

    Notes
    ----------
    This version should be functional but is missing several features that may be added later.
    Such as: 1) optional ability to check to see if runlblabc and runsmart scripts are already written
    to prevent overwrites, 2) parallelized version of calls to LBLABC through subprocess to take advantage
    of multiple cores on a machine, 3) example doc string, 4) include aerosols in runsmart file, 5)
    run in a batch mode with several photochemical input files (perhaps on Hyak), and 6) fix mismatch
    in LBLABC for HITRAN gases with indices above 37, though this may be better tackled by altering
    LBLABC itself.


    Examples    
    ----------  

    '''
    #---------import necessary modules---------------------------------------------------#

    #tell us what you're doing
    print 'importing modules'
    from read_photo import read_photo
    from rdlblin import rdlblin
    from rdhazecldin import rdhazecldin
    from rdsmartin import rdsmartin
    from write_lblabc import write_lblabc
    from write_smart import write_smart
    from gas_info import gas_info
    from haze_photo_smart import haze_photo_smart
    gases = gas_info()
    import subprocess
    print 'modules successfully imported'
    

    #-------------------------------------------------------------------------------------#

    #--read_photo reads photochemical output, generates .atm file, and returns values--#

    print 'Reading and parsing ' + photfile + ', interpolating to new P grid'
    gas_code, atm_pos, molwgt, atm_file = read_photo(photfile,tag, atm_dir = atm_dir,
                                                     plvls = plvls )
    print 'New .atm file generated: ' + str(atm_file) + ''
    print 'Gases read (column in file): ' 
    for i in range(len(gas_code)):
        print 'Gas code: '+ str(gas_code[i]) + ' Column: ' + str(atm_pos[i])
    print 'Molecular weight of atmosphere: ' + str(molwgt) + ''

    #--------------------------------------------------------------------------------------#

    #-----------writes lblabc scripts------------------------------------------------------#
 
    # load write_lblabc inputs from a file (unless default toggle set)
    if lbl_in_default:
        # no action necessary
        pass
    else:
        #load inputs for write_lblabc
        dmolwgt, minwn, maxwn, grav, radius, col_p, col_t, scaleP, n_t_prof,\
        t_offset, n_broad, scale_rmix, maxwidth, min_tau, out_dir, script_dir,\
        skip, rmix_type, par_file, hitran_tag, fundamntl_file, atm_dir2 \
        = rdlblin(filename = lblin_file)   
        # change atm_file if atm_dir is different
        if atm_dir == atm_dir2:
            atm_file_lbl = atm_file
        else:
            atm_file_lbl = atm_dir2 + tag + '.atm'
   
    # begin lists of script and .abs file strings
    script_files = []
    abs_files = []
    gas_absorbers = gas_code[2:]
    gas_cols = atm_pos[2:]
    for i in range(len(gas_absorbers)):
        index = int(gas_absorbers[i])
        # check if it is a hitran gas that needs .abs files - only do < 31 since there is a mismatch
        # between hitran and lblabc
        if (index <= 31 and index != 22 \
            and gases['maxwn'][index] >= minwn \
            and gases['minwn'][index] <= maxwn):
            #call write_lblabc
            print 'Writing runlblabc script for ' + gases['Formula'][index] + ''
            # use defaults if toggle is set
            if lbl_in_default:
                script_file, abs_file = write_lblabc(tag,atm_file,
                                    index,gas_cols[i],molwgt = molwgt)
            # use values from input text file
            else:
                if difflbl: 
                    script_file, abs_file = write_lblabc(difflbltag, atm_file_lbl, index, gas_cols[i], 
                                                         molwgt = molwgt, minwn = minwn, maxwn = maxwn, 
                                                         grav = grav, radius = radius, col_p = col_p, 
                                                         col_t = col_t, scaleP = scaleP, n_t_prof = n_t_prof,
                                                         t_offset = t_offset, n_broad = n_broad, scale_rmix = scale_rmix,
                                                         maxwidth = maxwidth, min_tau = min_tau, out_dir = out_dir, 
                                                         script_dir = script_dir, skip = skip, rmix_type = rmix_type,
                                                         par_file = par_file, hitran_tag = hitran_tag, 
                                                         fundamntl_file = fundamntl_file, shortfname=shortfname )
                if not difflbl: 
                    script_file, abs_file = write_lblabc(tag, atm_file_lbl, index, gas_cols[i], 
                                                         molwgt = molwgt, minwn = minwn, maxwn = maxwn, 
                                                         grav = grav, radius = radius, col_p = col_p, 
                                                         col_t = col_t, scaleP = scaleP, n_t_prof = n_t_prof,
                                                         t_offset = t_offset, n_broad = n_broad, scale_rmix = scale_rmix,
                                                         maxwidth = maxwidth, min_tau = min_tau, out_dir = out_dir, 
                                                         script_dir = script_dir, skip = skip, rmix_type = rmix_type,
                                                         par_file = par_file, hitran_tag = hitran_tag, 
                                                         fundamntl_file = fundamntl_file, shortfname=shortfname )
                    
            # update list of list of script strings
            script_files.append(script_file)
            abs_files.append(abs_file)
            print 'Wrote runlblabc script: ' + script_file + ''

    #---------------------------------------------------------------------------------------#
   
    #----------runs lblabc scripts to generate .abs files-----------------------------------#
    if genlblabc:
        print 'Looping through runlblabc scripts to generate .abs files'
        for i in range(len(abs_files)):
            #check to see if .abs file exists
            print 'Checking if ' + abs_files[i] + ' exists'
            try:
                command = ['ls', abs_files[i]]
                test_p = subprocess.check_call(command)
            #do this if .abs file does not exist
            except:
                print '.abs file does not already exist'
                #open script file as input to stdin and output file for writing to stdout
                input_file = open(script_files[i],'r')
                output_file = open(lblabc_out,'a')
                print 'generating .abs file for ' + script_files[i] + ''
                #try creating the .abs file
                try:
                    p = subprocess.check_call(lblabc_exe,stdin = input_file,
                                    stdout = output_file)
                #if it fails for some reason
                except: 
                    print 'FAILURE: .abs file for ' + script_files[i] + 'not generated!'
                # if it succeeds
                else:
                    print 'SUCCESS: .abs file for ' +script_files[i] + ' complete!'     
                #clean up
                finally: 
                    input_file.close()
                    output_file.close()
             #if the .abs file already exists
            else:
                print '.abs file for ' + script_files[i] + ' exists. Returning'   
    #----------------------------------------------------------------------------------------#


     #recall subprocess.Popen - at some point I will use this to parallize the lblabc runs so we can
     #take advantage of multiple processors on a machine.
             

    #----------------generates runsmart script------------------------------------------------#

    print 'Generating runsmart script for case ' + tag + ''
    # load write_smart inputs from a file (unless default toggle set)
    if smart_in_default:
        num_aero = 0.
        smart_script, smart_file = write_smart(tag, atm_file, t_surf, gas_absorbers, gas_cols,
                                            molwgt = molwgt)    
    else:
        # load input values from smartin.txt us rdsmartin
        t_surf, atm_p_col, atm_t_col, scaleP, minwn, maxwn, grav, radius,\
        dmolwgt, atm_skip, alb_file, alb_skip, r_AU, rmix_type, nstream, source,\
        scale_rmix, spec, spec_skip, spec_unit, specx, specx_scale, spec_wn_col,\
        spec_flux_col, n_sza, za, aa, con_crit, out_format, n_aa, aa2, out_level, out_unit, \
        grid_type, response, FWHM, sample_res, err_tau, err_pi0, err_g, err_alb, \
        out_format2, script_dir, hitran_tag, out_dir, xsec_dir, abs_dir, atm_dir3 = \
        rdsmartin(filename = smartin_file)

    #load aerosol inputs
    hcaer, cirrus, stratocum, hcaertaudir, hcaermiedir, cldtaudir, cldmiedir = \
    rdhazecldin(filename = hazecldin_file)

    num_aero = 0.
    if hazecloud:
        if hcaer.lower()== 'yes' or cirrus.lower() == 'yes' or stratocum.lower() == 'yes':
            if hcaer.lower() == 'yes': num_aero = num_aero + 18
            if cirrus.lower() == 'yes': num_aero = num_aero + 1
            if stratocum.lower() == 'yes': num_aero = num_aero + 1
            print 'number of aerosols is: '+str(num_aero)

            if hcaer.lower() == 'yes': 
                if gencloud:
                    print 'creating hydrocarbon aerosol cloud files'
                    hcaerfiles = haze_photo_smart(hcaerfile, forwardscatt = forwardscatt, debuggy = 0, makeplots = makeplots, doit = 1, fractally=fractally)
    else:
        hcaer = 'no'
        cirrus = 'no'
        stratocum = 'no'

    if not gencloud: hcaerfiles = []

    if not hazecloud:
        hcaerfiles = ''


    # check to see if user wants to read .atm files from same directory he/she is writing to
    # if not use new directory from smartin_file
    if (atm_dir) == (atm_dir3):
        atm_file_smart = atm_file
    else: 
        atm_file_smart = atm_dir3 + tag + '.atm'

        # write SMART file
        if difflbl: smart_script, smart_file = write_smart(tag, difflbltag, atm_file_smart, t_surf, gas_absorbers, gas_cols,
                                                           molwgt = molwgt, atm_p_col = atm_p_col, 
                                                           atm_t_col = atm_t_col, scaleP = scaleP, minwn = minwn, 
                                                           maxwn = maxwn, grav = grav, radius = radius, atm_skip = atm_skip,
                                                           alb_file = alb_file, alb_skip = alb_skip, r_AU = r_AU,
                                                           rmix_type = rmix_type, nstream = nstream, source = source, 
                                                           scale_rmix = scale_rmix, spec = spec, spec_skip = spec_skip,
                                                           spec_unit = spec_unit, specx = specx, specx_scale = specx_scale,
                                                           spec_wn_col = spec_wn_col, spec_flux_col = spec_flux_col, 
                                                           n_sza = n_sza, za = za, aa = aa, con_crit = con_crit, out_format = out_format,
                                                           n_aa = n_aa, aa2 = aa2, out_level=out_level, out_unit = out_unit, grid_type = grid_type, 
                                                           response = response, FWHM = FWHM, sample_res = sample_res, err_tau = err_tau,
                                                           err_pi0 = err_pi0, err_g = err_g, err_alb = err_alb,
                                                           out_format2 = out_format2, script_dir = script_dir, hitran_tag = hitran_tag,
                                                           out_dir = out_dir, xsec_dir = xsec_dir, abs_dir = abs_dir,  num_aero=num_aero, 
                                                           hcaer = hcaer, cirrus = cirrus, stratocum = stratocum, hcaertaudir= hcaertaudir,
                                                           hcaermiedir = hcaermiedir, cldtaudir = cldtaudir, cldmiedir = cldmiedir, hcaerfiles=hcaerfiles,
                                                           hcaerfile=hcaerfile, gencloud=gencloud, fractally=fractally, shortfname=shortfname)

        if not difflbl: smart_script, smart_file = write_smart(tag, tag, atm_file_smart, t_surf, gas_absorbers, gas_cols,
                                                           molwgt = molwgt, atm_p_col = atm_p_col, 
                                                           atm_t_col = atm_t_col, scaleP = scaleP, minwn = minwn, 
                                                           maxwn = maxwn, grav = grav, radius = radius, atm_skip = atm_skip,
                                                           alb_file = alb_file, alb_skip = alb_skip, r_AU = r_AU,
                                                           rmix_type = rmix_type, nstream = nstream, source = source, 
                                                           scale_rmix = scale_rmix, spec = spec, spec_skip = spec_skip,
                                                           spec_unit = spec_unit, specx = specx, specx_scale = specx_scale,
                                                           spec_wn_col = spec_wn_col, spec_flux_col = spec_flux_col, 
                                                           n_sza = n_sza, za = za, aa = aa, con_crit = con_crit, out_format = out_format,
                                                           n_aa = n_aa, aa2 = aa2, out_level=out_level, out_unit = out_unit, grid_type = grid_type, 
                                                           response = response, FWHM = FWHM, sample_res = sample_res, err_tau = err_tau,
                                                           err_pi0 = err_pi0, err_g = err_g, err_alb = err_alb,
                                                           out_format2 = out_format2, script_dir = script_dir, hitran_tag = hitran_tag,
                                                           out_dir = out_dir, xsec_dir = xsec_dir, abs_dir = abs_dir,  num_aero=num_aero, 
                                                           hcaer = hcaer, cirrus = cirrus, stratocum = stratocum, hcaertaudir= hcaertaudir,
                                                           hcaermiedir = hcaermiedir, cldtaudir = cldtaudir, cldmiedir = cldmiedir, hcaerfiles=hcaerfiles,
                                                           hcaerfile=hcaerfile, gencloud=gencloud, fractally=fractally, shortfname=shortfname)




    print 'Wrote runsmart script: ' + smart_script + ''
    #------------------------------------------------------------------------------------------#

    #-----runs SMART to generate output--------------------------------------------------------#
    if runsmart:
        print 'Check to see if SMART output already exists: ' + smart_file + '*'
        #check to see if SMART output already written
        try:
            command = ['ls', (smart_file + '*')]
            test_p = subprocess.check_call(command)
        #do this if SMART output does not yet exist
        except: 
            try:
                print 'Calling SMART for run: ' + smart_file + ''
                input_file = open(smart_script, 'r')
                output_file = open(smart_out, 'a')
                p = subprocess.check_call(smart_exe, stdin = input_file,
                                  stdout = output_file)
               #if it fails for some reason
            except: 
                print 'FAILURE: SMART run for ' + smart_script + ' not complete!'
            # if it succeeds
            else:
                # note that this doesn't mean SMART successfully completely a run only that the call above worked
                # what we really need to do is check again to make sure the output exists
                print 'SUCCESS: SMART run complete: ' + smart_file + ''  
                print 'If SMART ouput does not exist, check: ' + smart_out
                print 'Have an EXCELLENT day :)'
            #clean up
            finally: 
                input_file.close()
                output_file.close()
        #if the SMART output does exist
        else:
            print 'SMART output already exists: ' + smart_file + '*'
            print 'Returning.'
    #------------------------------------------------------------------------------------------#

 
         
 
