# -*- coding: iso-8859-1 -*-
def mass_r_b(r, r_b, rho, sigma, Nfactor):
   """
   Parameters
   ----------
   r : float
     radius in the lognormal distribution that we are integrating over
     Ex: 4.7e-6 [cm]
   r_b : float
     characteristic radius of the bin mode we are dealing with
     Ex: 5.0e-7 [cm]
   rho: float
     mass density of haze material
     this value is set in the main code at 0.63 g/cm^3 (see Trainer et al 2006)
   sigma : float
     Dimensionless geometric standard deviation for aerosol distribution
     Defined in the main code as ln(1.5) (see http://dust.ess.uci.edu/facts pg 7-8 of particle distributions pdf) 
   Nfactor : Number density conversion factor defined in main code 
     * This is how we are able to solve for the number density of the larger radius bin in terms of smaller radius bin 
       while conserving mass of the layer
     * In the main code, Number density of larger radius bin = Nfactor*Number density of smaller radius bin.
       Ask Giada if you want to see why this was done this way (it's because we don't have enough information to solve for 
       the number density of each mode independently)
     !!!!VERY important: when this code is calculating for the smaller bin, Nfactor MUST be defined as = 1!
                     when this code is calculating for the larger bin, Nfactor is defined as a radio of the
                     log space distances between the two bins (see main code)     
     Note: Nfactor in this context is NOT the same as N_r in the code below
     

   Returns
   ----------
   mass_atm : float
      Calculates  mass of atmospheric layer's haze / length  (this is an integrand, not the final quantity, hence the funky units)
      Ex: 1.33962e-17 g/cm
      Ultimately used to calculate mass of atmospheric layer's lognormal distribution of particles [units of grams]

    Notes
    ---------- 
    This is the integrand to calculate mass of atmos layer based on lognormal distribution to allow us to find number density of each particle mode
    in a given atmospheric layer.
    The integrand which is calculated here has units g/cm; then multiplying it times dr [cm] yields just grams
    (note: recall log(x) - log(y) = log(x/y) so the exp(log(blah)) part is actually unitless)

    Revision History
    ---------- 
    Written by E. Schwieterman and G. Arney August, 2014
    Adapted from IDL codes written by G. Arney

    Examples   
    ---------- 


   """
   #---import numpy ------------#
   from numpy import pi
   from numpy import log
   from numpy import sqrt
   from numpy import exp
   #----------------------------#

   #-----calculate intermediate variables--------#
   m =  rho * (4./3.)* pi *r**3.
   aa =  1./sqrt(2*pi) * 1./sigma 
   bb = 2.*sigma**2.
   #---------------------------------------------#

   #------calculate mass-----------------------------------------------------------#
   mass_atm =   Nfactor * 1./r * aa * m * exp(-((log(r) - log(r_b))**2.) / bb)
   #-------------------------------------------------------------------------------#

   #return mass
   return mass_atm


def area_r_b(r, r_b, rho, sigma, N_r, ind):
   """
   
   Parameters
   ----------
   r : float
     radius of the aerosol we're integrating over
     Ex: 4.7e-7 [cm]
   r_b : float
     radius of bin mode we are dealing with 
     Ex: 5.0e-7 [cm]
   rho: float
     mass density of haze material
     this value is set in the main code at 0.63 g/cm^3 (see Trainer et al 2006)
   sigma : float
     Dimensionless geometric standard deviation for aerosol distribution
     Defined in the main code as log(1.5) in accordance with the literature's definition of a "reasonable" value
   N_r : float
     Number density of particles of radius bin r [particles/cm**3]
     Reasonable values range from 1e-7 to 1e5 or so but smaller or larger values are possible depending on the 
     photochemical model input parameters
   ind : complex floats
     Tholin complex refractive index
     **This MUST be the refractive index at 1um since 1um is the arbitrary reference wavelength used here.** 
     Defined in the main code as [1.65,1e-3] which are the real and imaginary refractive indices, respectively, at 1um.

   Returns
   ----------
   B : float
  
    Notes
    ---------- 
    B = the area * number density * extinction at the reference wavelength of the lognormal particle distribution
    This code is only to be used for spherical particles becuase it uses mie scattering physics.
    We use B to find the optical depth of a given atmospheric layer in the main code.

    Revision History
    ---------- 
    Written by E. Schwieterman and G. Arney August, 2014
    Adapted from IDL codes written by G. Arney

    Examples   
    ---------- 
   """
   #---import stuff ------------#
   from numpy import pi
   from numpy import log
   from numpy import sqrt
   from numpy import exp
   from pymiecoated import Mie
   #----------------------------#

   #-----calculate intermediate variables--------#
   area = pi * r**2.
   aa =  1./sqrt(2.*pi) * 1./sigma 
   bb = 2.*sigma**2.
   dx = (2.*pi*r)/(1.0/1.e4)#1e-4 = um per cm 1 um reference wavelength)
   mie = Mie(x=dx, m=ind)
   dqext = mie.qext()
   #---------------------------------------------#

   #------calculate area-----------------------------------------------------------#
   B =  dqext * N_r * 1/r * aa * area * exp(-((log(r) - log(r_b))**2) / bb)
   #-------------------------------------------------------------------------------#

   #return area
   return B

def func_N(r, r_b, sigma, N_r):
   """
   Parameters
   ----------
  r : float
     radius of the aerosol we're integrating over
     Ex: 4.7e-7 [cm]
   r_b : float
     radius of bin mode we are dealing with 
     Ex: 5.0e-7 [cm]
   rho: float
     mass density of haze material
     this value is set in the main code at 0.63 g/cm^3 (see Trainer et al 2006)
   sigma : float
     Dimensionless geometric standard deviation for aerosol distribution
     Defined in the main code as log(1.5) in accordance with the literature's definition of a "reasonable" value
   N_r : float
     Number density of particles of radius bin r [particles/cm**3]
     Reasonable values range from 1e-7 to 1e5 or so but smaller or larger values are possible depending on the 
     photochemical model input parameters

   Returns
   ----------
   func_N : float
      Area ... ?   
      Ex: ???

    Notes
    ---------- 
    Calculates density distribution based on lognormal distribution 
    NOTE: I just included this for completeness, but this is never actually used in the main code

    Revision History
    ---------- 
    Written by E. Schwieterman and G. Arney August, 2014
    Adapted from IDL codes written by G. Arney

    Examples   
    ---------- 

   """

   #---import ------------------#
   from numpy import pi
   from numpy import log
   from numpy import sqrt
   from numpy import exp
   #----------------------------#
   #-----calculate intermediate variables--------#
   aa =  1./sqrt(2.*pi) * 1./sigma 
   bb = 2.*sigma**2.
   #---------------------------------------------#
  
   #-------------calculate number----------------------------------------#
   num =   N_r * 1./r * aa * exp(-((log(r) - log(r_b))**2.) / bb)
   #---------------------------------------------------------------------#

   return num
