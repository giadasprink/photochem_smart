278.0                                              # surface temperature
1                                                  # column containing pressure
2                                                  # column containing temperature 
1.0                                                # pressure scaling factor 
400                                                # minimum wavenumber
2500                                               # maximum wavenumber
9.8                                                # gravitational acceleration
6371.                                              # planetary radius (km)
29.                                                # default molecular weight
1                                                  # number of lines to skip at top of .atm file
fixed_input/albedo/seawater.alb                    # path and name of albedo file
27                                                 # lines to skip at top of albedo file
1.0                                                # distance to star in AU (used to scale stellar spectrum)
1                                                  # mixing ratio type (1-volume, 2-mass)
8                                                  # number of streams
3                                                  # source - Thermal (1), Solar (2), Both (3)
1.0                                                # scaling factor for mixing ratio in atm
fixed_input/specs/Kurucz1cm-1_susim_atlas2.dat     # location of stellar spectrum
12                                                 # lines to skip at top of stellar spectrum file
1                                                  # stellar spectrum units 1-[W/m*m/cm], 2-[W/m*m/nm], 3-[W/m*m/um]
2                                                  # stellar spectrum abscissa type - 1)-wavelength, 2)-wavenumber
1.0                                                # scaling factor for stellar spectrum absciss
1                                                  # column number of spectrum file with wavelength/wavenumber
2                                                  # column number of spectrum file with spectral flux
1                                                  # number of solar zenith angles
60.                                                # solar zenith angle
0.                                                 # azimuth angle
0.01                                               # convergence criterion
1                                                  # output format
1                                                  # number of azimuth angles
0.                                                 # azimuth angle
2                                                  # output units 1 -[W/m*m/sr/wn], 2-[W/m*m/sr/um], 3 - [2/m*m/sr/nm], 4-[W/m*m/sr/Ang]
2                                                  # grid type - 2 - slit
2                                                  # response function 2- triangular
1.0                                                # FWHM (wavenumbers)
1.0                                                # sampling resolution (per cm^-1)
0.25                                               # optical depth relative binning error (tau error binning)
0.15                                               # co-single scattering albedo absolute binning error (pi error binning)
0.15                                               # g error binning
0.02                                               # albedo error binning
1                                                  # output format 1 - ascii, 2 - binary
smart_io/runsmart/                                 # directory to place runsmart scripts
hitran2012                                         # database tag for script names and output strings
smart_io/output/                                   # output directory for SMART runs
fixed_input/xsec/                                  # directory where cross section files reside
lblabc_io/output/                                  # directory to look for .abs absorption coefficient files
