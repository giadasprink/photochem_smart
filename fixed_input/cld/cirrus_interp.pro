pro cirrus_interp

  filename1 = 'Solar_mix1_d100.dat'
  filename2 = 'IR_mix1_d100.dat'
  wl_interp = [2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0]
  READCOL, filename1, FORMAT = 'f,f,f,f,f,f,f,f,f,f,f,f,f', $
                      wl_sol, omega_sol, std_omega_sol, g_sol, std_g_sol, $
                      fd_sol, std_fd_sol, Qe_sol, std_Qe_sol, sig_scat_sol, $
                      std_sig_scat_sol, sig_ext_sol, std_sig_ext_sol
  READCOL, filename2, FORMAT = 'f,f,f,f,f,f,f,f,f,f,f', $
                      wnum_ir, omega_ir, std_omega_ir, g_ir, std_g_ir, $
                      Qe_ir, std_Qe_ir, sig_scat_ir, std_sig_scat_ir, $
                      sig_ext_ir, std_sig_ext_ir
  wl_ir  = (1.e4)/wnum_ir
  wl_ir = ROTATE(wl_ir,2)
  omega_ir = ROTATE(omega_ir,2)
  g_ir = ROTATE(g_ir,2)
  Qe_ir = ROTATE(Qe_ir,2)
  sig_scat_ir = ROTATE(sig_scat_ir,2)
  sig_ext_ir = ROTATE(sig_ext_ir,2)

  OPENW, 1, 'baum_cirrus_de100.mie'

  wl_old = [wl_sol,wl_ir]
  omega = [omega_sol,omega_ir]
  g = [g_sol,g_ir]
  Qe = [Qe_sol,Qe_ir]
  sig_scat = [sig_scat_sol,sig_scat_ir]
  sig_ext = [sig_ext_sol,sig_ext_ir]
  wl = [wl_sol,wl_interp,wl_ir]

  omega = SPLINE(wl_old,omega,wl)
  g = SPLINE(wl_old,g,wl)
  Qe = SPLINE(wl_old,Qe,wl)
  sig_scat = SPLINE(wl_old,sig_scat,wl)
  sig_ext = SPLINE(wl_old,sig_ext,wl)

  result = size(wl)
  n_wl = result[1]

  for i=0, n_wl-1 do begin
    printf, 1, wl[i], omega[i], g[i], Qe[i], omega[i]*Qe[i];, sig_scat[i], sig_ext[i]
  endfor

  CLOSE, 1

end
