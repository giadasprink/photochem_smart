;reads cross sections file and removes negative numbers

pro filtneg, infilename, outfilename,skipline=skipline

readcol,infilename,wl,xsec,skipline=skipline

;find values where xsec is positive
i = where(xsec > 0.)

;refill arrays with only positive values
wl = wl[i]
xsec = xsec[i]

;write to file
writecol, outfilename, wl, xsec, fmt='(5X,E11.5,7X,E11.5)'

end
