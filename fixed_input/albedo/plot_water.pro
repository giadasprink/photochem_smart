;compare albedo plots of seawater.alb and ocean.alb

pro plot_water

readcol, 'seawater.alb', wls, albs, skipline=27
readcol, 'ocean.alb', wlo, albo, skipline=27

filename='compare_sea_ocean.pdf'

ps_start, filename=filename

@edstyle
@colors_kc
mu=greek('mu')
plot, wlo,albo, xtitle='Wavelength ['+mu+'m]', ytitle='Albedos',$
                 xrange=[0.2,3.0], yrange=[0.,0.4], $
                 title = 'Albedo File Comparison'
oplot,wls,albs, line=2
al_legend, ['ocean.alb','seawater.alb'], line=[0,2], position=[1.5,0.3]


ps_end

spawn, 'gv '+filename+' &'
end
