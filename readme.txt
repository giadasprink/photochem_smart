
Description for smart_interface.py (found in photochem_smart/interface/)

***Interfaces Photochemistry output with SMART***

Together with the imported modules this code will take a photochemical model output file read
and parse the P-T structure and gas mixing ratios, generate a new .atm file with a new pressure
grid equally spaced in logarithmic pressure space, interpolate the gas mixing ratios to this new
pressure grid, write runlblabc scripts for each HITRAN gas, run LBLABC for each runlblabc script,
generate a runsmart script (no aerosol scenario only for now), and run SMART. The user defined
parameters for LBLABC and SMART are given in text files read by rdlblin and rdsmartin. Note the
large number of keyword options there are for each submodule. 

This version should be functional but is missing several features that may be added later.
Such as: 1) optional ability to check to see if runlblabc and runsmart scripts are already written
to prevent overwrites, 2) parallelized version of calls to LBLABC through subprocess to take advantage
of multiple cores on a machine, 3) some examples doc strings in the .py files, 4) include aerosols in runsmart file, 
5)run in a batch mode with several photochemical input files (perhaps on Hyak), and 6) fix mismatch
in LBLABC for HITRAN gases with indices above 37, though this may be better tackled by altering LBLABC itself.

----------------------------------------------------------------------------------------------------------------

Running the code should be be as simple as:

from interface import smart_interface
photfile = 'photchem/Sun_30.0SorgFlux_30000ppmCO2_1.0CH4flux.profile.pt'
tag = 'Sun_30.0SorgFlux_30000ppmCO2_1.0CH4flux'
smart_interface(photfile, tag)

from interface import smart_interface
photfile = 'photchem/profile.pt'
tag = 'test'
smart_interface(photfile, tag)


Which should work for the example photochemical model output file given in the package. However, there are a great many optional parameters
that can be changed either in the call to smart_interface or in the LBLABC and SMART input files (more on those later) 

The code assumes that the photfile is a photochemistry output file with columns of pressure, temperature, and gas mixing ratios.
The first line in the file should be a header containing strings to denote Pressure ('Press'), Temperature ('Temp') and gas formulae
that are capitalized in the same format the HITRAN database uses. Here is an example:

   Alt      Temp       Den      Press     H2O        CH4       C2H6      CO2      O2        O3         DMDS      DMS       CH3SH    CS2       OCS
5.000E+04 2.750E+02 2.479E+19 9.409E-01 5.187E-03 9.244E-04 1.334E-05 3.000E-02 5.035E-14 7.913E-22 3.521E-09 3.579E-09 9.871E-10 2.563E-12 7.197E-09
1.500E+05 2.670E+02 2.257E+19 8.315E-01 2.700E-03 9.244E-04 1.334E-05 3.000E-02 5.685E-14 5.181E-21 2.265E-09 1.200E-09 4.393E-10 4.952E-13 7.121E-09
2.500E+05 2.590E+02 2.048E+19 7.321E-01 1.324E-03 9.244E-04 1.334E-05 3.000E-02 5.929E-14 3.228E-20 8.244E-10 2.769E-10 1.516E-10 1.797E-13 6.752E-09
3.500E+05 2.500E+02 1.861E+19 6.419E-01 5.625E-04 9.243E-04 1.334E-05 3.000E-02 4.654E-14 2.010E-19 9.503E-11 2.575E-11 2.295E-11 4.117E-14 5.807E-09
4.500E+05 2.420E+02 1.678E+19 5.602E-01 2.489E-04 9.243E-04 1.334E-05 3.000E-02 3.960E-14 5.838E-19 4.074E-12 9.479E-13 1.113E-12 2.165E-15 4.598E-09
....
....

Extraneous columns like Alt or Den are ignored. So would any gases that are not found in the gas_info module (source info: interface/fixed_input/gases.csv)
WARNING: a gas labeled, e.g. OSC instead of OCS would also be ignored.

The module read_photo will read this file in, parse it (recognizing the gas info for each gas such as molecular weight and HITRAN gas index), create a new
pressure grid (replacing pgrid.f), interplolate the temperature profile and mixing ratio profiles to this new pressure grid (replacing xyinterp.f), and writes
the new data to an .atm file named after the tag string above (replacing profile.f). What's more, it will keep track of gas indices and column positions in 
the new .atm file for writing runlblabc and runsmart scripts. Not only that, but it will use N2 as a filler gas (already assumed I believe in the Kasting climate 
model) and dynamically calculate the mean molecular weight of the atmosphere.

The goal of the code of course is to make generating spectra based on the model atmospheres as painless as possible, so almost everything that can be pulled from
the .pt file is so you don't have to spend a bunch of time editing scripts. However as you know there are a spectacular number of things you might want to change 
in run based on information not in the input file, such as the stellar spectrum. Another possibility is the minimum and maximum wavenumber range for the output
spectra, which largely sets the computation time for LBLABC and SMART.

The runlblabc scripts will be generated based off the info from the photfile and the settings given in the lblabc input parameter file (default location: 
lblabc_io/lblin.txt). This input file is read by rdlblin.py and the runlblabc file itself is generated by write_lblabc.py.

The runsmart scripts will similarly be generated based on this info and the values in the smart input parameter file (default location:
smart_io/smartin.txt). This input file is read by rdsmartin.py and the runsmart file is generated by write_smart.py.

There are toggles in smart_interface to use the defaults values for the scripts, which should be the same as those that are hard coded
in the smart_interface bash script (i.e., r_AU = 1.0 (distance from the star)). See the smart_interface.py documentation for all the options.

LBLABC and SMART are called by the subprocess module inside the smart_interface.py code. There are try, except, else, and print commands
to let you know via the command line how the run is going. The standard output for LBLABC and SMART is directed to a file like smart_out.txt 
and lblabc_out.txt, so if the run fails you can check the reason. 

There are still a lot of things that can be done to improve this package. One thing that should be done soon is to finish adding the UV cross sections
for all gases in fixed_input/xsec/. I haven't had time to do this yet because there are so many, so please feel free if you want to contribute :)

I encourage you to look at the EXTREMELY WELL COMMENTED ;) code, especially the header information for each of the modules:

    read_photo.read_photo
    rdlblin.rdlblin
    rdsmartin.rdsmartin
    write_lblabc.write_lblabc
    write_smart.write_smart
    gas_info.gas_info

..all found in interface/

Directory Structure
-------------------------
Here are all the folders and subfolders in the package.

atm : .atm files generated by read_photo from the photochemical model output file
fixed_input --> xsec/ (cross sections), albedo/ (albedo files), spec/ (stellar spectra), cld/ (cloud and scattering parameter files)
interface --> all of the modules listed above that allow smart_interface to function (each can be used seperately too)
lblabc_io --> output/ (.abs files), runlblabc/ (lblabc scripts), also lblin.txt (contains parameters for runlblabc scripts)
photchem --> output from the photochemistry model that serves as input for smart_interface
smart_io --> output/ (output spectra, heating rate files, etc), runsmart/ (smart scripts), also smartin.txt (contains parameters for runsmart)
readme.txt --> YOU ARE HERE

I have left the toa.rad script for my SMART run with the Sun_30.0SorgFlux_30000ppmCO2_1.0CH4flux input. You might want to make sure yours matches. 

I would be interested in any comments or suggestions you have about the code! - Eddie (eschwiet@uw.edu)
